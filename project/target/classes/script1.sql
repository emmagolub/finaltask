INSERT INTO `educationCenter`.`Authorization` (`Login`, `Password`, `Role`, `Status`) 
VALUES ('emmagolub', '9da99e25410d8ab2564d5621c260f419d2a75', true, false); 

INSERT INTO `educationCenter`.`Authorization` (`Login`, `Password`, `Role`, `Status`) 
VALUES ('user', '9da99e25410d8ab2564d5621c260f419d2a75', false, false); 

INSERT INTO `educationCenter`.`Authorization` (`Login`, `Password`, `Role`, `Status`) 
VALUES ('user1', '9da99e25410d8ab2564d5621c260f419d2a75', false, true); 

INSERT INTO `educationCenter`.`Employee` (`Name`, `Date`, `occupation`, `NumberOfChildren` , `SingleParent`, `CNPP`) 
VALUES ('Борисевич Алексей Владимирович', '2015-01-01', 'Генеральный директор', 1, false, false); 

INSERT INTO `educationCenter`.`Employee` (`Name`, `Date`, `department`, `occupation`, `NumberOfChildren` , `SingleParent`, `CNPP`) 
VALUES ('Петров Петр Петрович', '2015-01-01', 'Отдел кадров', 'Начальник отдела', 2, false, false); 

INSERT INTO `educationCenter`.`Employee` (`Name`, `Date`, `department`, `occupation`, `NumberOfChildren` , `SingleParent`, `CNPP`) 
VALUES ('Алексеев Алексей Алексеевич', '2015-01-01', 'Отдел кадров', 'Менеджер по подбору персонала', 1, true, false); 

INSERT INTO `educationCenter`.`Employee` (`Name`, `Date`, `department`, `occupation`, `NumberOfChildren` , `SingleParent`, `CNPP`) 
VALUES ('Федоров Федор Федорович', '2015-01-01', 'Отдел маркетинга', 'Маркетолог', 0, false, true); 

INSERT INTO `educationCenter`.`Employee` (`Name`, `Date`, `occupation`, `NumberOfChildren` , `SingleParent`, `CNPP`) 
VALUES ('Сидоров Иван Петрович', '2015-01-01',  'Преподаватель английского языка', 1, false, false); 


INSERT INTO `educationCenter`.`salary` (`EmployeeNumber`, `Date`, `Sum`) 
VALUES (1, '2015-02-25', 8700000); 

INSERT INTO `educationCenter`.`salary` (`EmployeeNumber`, `Date`, `Sum`) 
VALUES (2, '2015-02-25', 6000000); 

INSERT INTO `educationCenter`.`salary` (`EmployeeNumber`, `Date`, `Sum`) 
VALUES (3, '2015-02-25', 3100000); 

INSERT INTO `educationCenter`.`salary` (`EmployeeNumber`, `Date`, `Sum`) 
VALUES (4, '2015-02-25', 2500000); 

INSERT INTO `educationCenter`.`salary` (`EmployeeNumber`, `Date`, `Sum`) 
VALUES (5, '2015-02-25', 5000000); 

INSERT INTO `educationCenter`.`salary` (`EmployeeNumber`, `Date`, `Sum`) 
VALUES (1, '2015-03-25', 87000000); 

INSERT INTO `educationCenter`.`salary` (`EmployeeNumber`, `Date`, `Sum`) 
VALUES (2, '2015-03-25', 6000000); 

INSERT INTO `educationCenter`.`salary` (`EmployeeNumber`, `Date`, `Sum`) 
VALUES (3, '2015-03-25', 3100000); 

INSERT INTO `educationCenter`.`salary` (`EmployeeNumber`, `Date`, `Sum`) 
VALUES (4, '2015-03-25', 2500000); 

INSERT INTO `educationCenter`.`salary` (`EmployeeNumber`, `Date`, `Sum`) 
VALUES (5, '2015-03-25', 5000000); 


INSERT INTO `educationCenter`.`prepayment` (`EmployeeNumber`, `Date`, `Sum`) 
VALUES (1, '2015-03-01', 2600000); 

INSERT INTO `educationCenter`.`prepayment` (`EmployeeNumber`, `Date`, `Sum`) 
VALUES (2, '2015-03-02', 1000000); 

INSERT INTO `educationCenter`.`prepayment` (`EmployeeNumber`, `Date`, `Sum`) 
VALUES (3, '2015-03-03', 1500000); 

INSERT INTO `educationCenter`.`prepayment` (`EmployeeNumber`, `Date`, `Sum`) 
VALUES (4, '2015-03-01', 1400000); 

INSERT INTO `educationCenter`.`prepayment` (`EmployeeNumber`, `Date`, `Sum`) 
VALUES (5, '2015-03-06', 1200000);


INSERT INTO `educationCenter`.`vacation` (`EmployeeNumber`, `Date`, `Sum`, `Period`) 
VALUES (1, '2015-03-01', 4600000, '2015-03-25 // 2015-04-23'); 
/*
INSERT INTO `educationCenter`.`Company` (`Name`, `Adress`, `Telephone`, `CEO`, `ChiefAccountant`, `INN`, `Bank`, `CheckingAccount`) 
VALUES ('Образовательный центр "New Education"', 'г. Минск, ул. Фрунзенская, д.15', '+375 17 222 22 22', 
'Борисевич Алексей Владимирович', 'Примшиц Наталья Алексеевна', '990126355698', 'Беларусбанк', '12385497654891230045'); 
*/

INSERT INTO `educationCenter`.`IncomeTax` (`StandartPercent`, `MinimumBet`, `MinimumAmount`, `AmountPerChild`, `AmountForSingleParent`, `AmmountForAffectedByCNPP`, `PensionFund`) 
VALUES (13, 730000, 4420000, 210000, 410000, 1030000, 1); 
