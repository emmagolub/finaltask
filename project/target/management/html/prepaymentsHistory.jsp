<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="aTag" uri="/WEB-INF/taglib.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
  <fmt:setBundle basename="${bundle}" var="lang"/>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/info-table.css">
  <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/buttons.css">
  <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/styles.css">
  <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/history.css">
  <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/js/hamburger-menu.js"></script>

</head>
<title><fmt:message key="title.history" bundle="${lang}"/></title>

<body>

<div id="pgcontainer">
  <header>
    <div id="navbar">
      <a href="#" class="menubtn">menu</a>
      <div id="hamburgermenu">
        <ul>
          <li>
            <form name="changeLangForm" action="Control" method="get">
              <input type="hidden" name="cmd" value="changeLang">
              <input type="hidden" name="path" value="html/prepaymentsHistory.jsp">
              <input type="submit" class="locale-btn" name="langBtn" value="ru_Ru">
              <input type="submit" class="locale-btn" name="langBtn" value="en_En">
            </form>
          </li>
          <jsp:include page="menu.jsp"/>

        </ul>
      </div>
    </div>
    <div class="overlay"></div>
  </header>

  <div id="content">
    <form id="otp-form">
      <div id="wrap-form">

        <h1><fmt:message key="prepayment.history" bundle="${lang}"/></h1>
        <hr>
        <p id="message">${message}</p>
        <fmt:setLocale value="${bundle}"/>

        <div class="myTable">
          <table>
            <tr>
              <td><fmt:message key="td.payment.numeber" bundle="${lang}"/></td>
              <td><fmt:message key="td.kind" bundle="${lang}"/></td>
              <td><fmt:message key="td.name" bundle="${lang}"/></td>
              <td><fmt:message key="td.sum" bundle="${lang}"/></td>
              <td><fmt:message key="input.adding.date" bundle="${lang}"/></td>
            </tr>
            <c:forEach var="prepayment" items="${prepaymentsList}">
              <tr>
                <td><c:out value="${prepayment.id}"/></td>
                <td><fmt:message key="input.prepayment" bundle="${lang}"/></td>
                <td><c:out value="${prepayment.employee.name}"/></td>
                <td><c:out value="${prepayment.sum}"/></td>
                <td><fmt:formatDate value="${prepayment.date}" type="date" dateStyle="full" /></td>
              </tr>
            </c:forEach>
          </table>
        </div>
        <aTag:pagination action="Control" command="showPrepayments"
                         totalEntries="${totalEntries}" current="${currentTablePage}"
                         totalPages="${totalTablePages}"
                         pStyleClass="paginationP" btnClass="pagination-btn"/>
      </div>
    </form>

  </div>
</div>

</body>
</html>
