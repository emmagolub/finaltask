<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setBundle basename="${bundle}" var="lang"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/input.css">
    <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/buttons.css">
    <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/form.css">
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/updateEmpl.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/hamburger-menu.js"></script>

</head>
<title><fmt:message key="title.add.salary" bundle="${lang}"/></title>

<body>
<div id="pgcontainer">
    <header>
        <div id="navbar">
            <a href="#" class="menubtn">menu</a>

            <div id="hamburgermenu">
                <ul>
                    <li>
                        <form name="changeLangForm" action="Control" method="get">
                            <input type="hidden" name="cmd" value="changeLang">
                            <input type="hidden" name="path" value="html/dismissEmployee.jsp">
                            <input type="submit" class="locale-btn" name="langBtn" value="ru_Ru">
                            <input type="submit" class="locale-btn" name="langBtn" value="en_En">
                        </form>
                    </li>
                    <jsp:include page="menu.jsp"/>

                </ul>
            </div>
        </div>
        <div class="overlay"></div>
    </header>

    <div id="content">
        <h1><fmt:message key="h2.employee.dimissing" bundle="${lang}"/></h1>
        <hr>
        <h2 style="line-height: 150%"><fmt:message key="h4.description.dismiss.employee" bundle="${lang}"/></h2>
        <br>
        <p id="message">${message}</p>

        <div class="wrap-form">
            <form method="post" action="${pageContext.request.contextPath}/Control">
                <label><fmt:message key="input.personal.number" bundle="${lang}"/>:</label>
                <input type="text" pattern="^\d+$" class="input" required name="personalNumber"/>
                <label><fmt:message key="td.end.date" bundle="${lang}"/>:</label>
                <input type="date" class="input" name="dismissingDate" required/>
                <br>
                <input type="submit" class="control-button"
                       value="<fmt:message key="button.remove" bundle="${lang}"/>"/>
                <input type="hidden" name="cmd" value="dismissEmployee">
            </form>
        </div>

    </div>
</div>

</body>
</html>


