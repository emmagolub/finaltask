<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setBundle basename="${bundle}" var="lang"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/input.css">
    <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/buttons.css">
    <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/form.css">
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/updateEmpl.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/hamburger-menu.js"></script>


</head>
<title><fmt:message key="title.update.employee" bundle="${lang}"/></title>

<body>
<div id="pgcontainer">
    <header>
        <div id="navbar">
            <a href="#" class="menubtn">menu</a>

            <div id="hamburgermenu">
                <ul>
                    <li>
                        <form name="changeLangForm" action="Control" method="get">
                            <input type="hidden" name="cmd" value="changeLang">
                            <input type="hidden" name="path" value="html/updateEmployee.jsp">
                            <input type="submit" class="locale-btn" name="langBtn" value="ru_Ru">
                            <input type="submit" class="locale-btn" name="langBtn" value="en_En">
                        </form>
                    </li>
                    <jsp:include page="menu.jsp"/>

                </ul>
            </div>
        </div>
        <div class="overlay"></div>
    </header>

    <div id="content">
        <h1><fmt:message key="title.update.employee" bundle="${lang}"/></h1>
        <hr>
        <h2><fmt:message key="h4.description.update.employee" bundle="${lang}"/></h2>
        <br>
        <p id="message">${message}</p>

        <div class="wrap-form">
            <form method="get" action="${pageContext.request.contextPath}/Control">
                <input type="submit" class="control-button" value="<fmt:message key="button.show" bundle="${lang}"/>"/>
                <input type="hidden" name="cmd" value="employeeUpdateInfo">
                <input type="hidden" name="ID" id="ID">
            </form>

            <form method="post" action="${pageContext.request.contextPath}/Control">
                <label><fmt:message key="td.number" bundle="${lang}"/>:</label>
                    <input type="text" class="input" id="number" name="number" value="${number} " required="true"/>
                    <label><fmt:message key="td.name" bundle="${lang}"/>:</label>
                    <input type="text" class="input" name="name" value="${name}"/>
                    <label><fmt:message key="td.department" bundle="${lang}"/>:</label>
                    <input type="text" class="input" name="department" value="${department}"/>
                    <label><fmt:message key="td.occupation" bundle="${lang}"/>:</label>
                    <input type="text" class="input" name="occupation" value="${occupation}"/>
                    <label><fmt:message key="td.start.date" bundle="${lang}"/>:</label>
                    <input type="date" class="input" name="date" value="${date}"/>
                    <label><fmt:message key="td.children.number" bundle="${lang}"/>:</label>
                    <input type="number" pattern="^\d+" class="input" name="numberOfChildren" value="${numberOfChildren}"/>
                    <label><fmt:message key="td.single.parent" bundle="${lang}"/>:</label>
                    <input type="checkbox" class="input" name="singleParent" id="singleParent" value="${singleParent}"/>
                    <label><fmt:message key="td.cnpp" bundle="${lang}"/>:</label>
                    <input type="checkbox" class="input" name="cnpp" id="cnpp" value="${cnpp}"/>
                    <input type="submit" class="control-button"
                           value="<fmt:message key="button.update" bundle="${lang}"/>"/>
                    <input type="hidden" name="cmd" value="updateEmployee">
            </form>

        </div>

    </div>
</div>

</body>
</html>
