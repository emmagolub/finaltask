<!DOCTYPE HTML>
<html>
<head>
  <title>400 User error</title>
  <link href="${pageContext.request.contextPath}/css/400.css" rel="stylesheet" type="text/css"  media="all" />
</head>
<body>
<div class="wrap">
  <div class="header">
    <div class="logo">
      <h1><a href="#">Ohh</a></h1>
    </div>
  </div>
  <div class="content">
    <img src="${pageContext.request.contextPath}/img/error-img400.png" title="error" />
    <p><span><label>O</label>hh.....</span>You have filled wrong information in the form</p>
    <div class="copy-right">
      <p>&#169 All rights Reserved</p>
    </div>
  </div>
</div>
</body>
</html>

