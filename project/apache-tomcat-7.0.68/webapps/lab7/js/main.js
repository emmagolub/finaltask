function show(blockName){
    document.getElementById(blockName).style.display = 'block';
    document.getElementById('wrap').style.display = 'block';
}

function hide(){
    document.getElementById('addWindow').style.display = 'none';
    document.getElementById('delWindow').style.display = 'none';
    document.getElementById('wrap').style.display = 'none';
}

document.getElementById("wrap").addEventListener('click', function(){
    hide();
}, false);

document.getElementById("addStudent").addEventListener('click', function(){
    show('addWindow');
}, false);

document.getElementById("delStudent").addEventListener('click', function(){
    show('delWindow');
}, false);
