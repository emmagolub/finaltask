<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link type="text/css" rel="stylesheet" href="css/style.css"/>
    <link type="text/css" rel="stylesheet" href="css/table.css"/>
    <title>Students Control</title>
</head>

<body>

<div class="buttonDiv">
    <div>
        <form action="${pageContext.request.contextPath}\Control">
            <input class="btn" type="submit" value="Select" name="cmd"/>
            <input class="btn" type = "button" value = "Add" id = "addStudent" name="btn"/>
            <input class="btn" type = "button" value = "Delete" id = "delStudent" name="btn"/>
        </form>
    </div>
</div>

<%--<form action="${pageContext.request.contextPath}/Control" method="get">
    user name <input type="text" name="name"/>
    email <input type="text" name="email"/>
    <input type="submit" name="cmd" value="Hello"/>
</form>--%>

<div id="wrap"></div>
<!-- Само окно-->
<div id="addWindow">
    <form class="form-container" action="${pageContext.request.contextPath}/Control" method = "get">
        <div class="form-title"><h2>Add students</h2></div>

        <div class="form-title">Name:
            <input class="form-field" type="text"  name = "Name" placeholder="XXXXXXXX XXXXX XXXXXXXX"/><br /></div>

        <div class="form-title">Groups number:
            <input class="form-field" type="text"  name = "Groups number" placeholder="372302"/><br /></div>

        <div class="form-title">Average score:
            <input class="form-field" type="text"  name = "Average score" placeholder="8.2"/><br /></div>
        <div class="submit-container">
            <input class="submit-button" type="reset"  value = "Reset"/>
            <input class="submit-button" value="Add student" type="submit" name="cmd">
        </div>
    </form>
</div>

<div id="delWindow">
    <form class="form-container" action="${pageContext.request.contextPath}/Control" method = "get">
        <div class="form-title"><h2>Delete students</h2></div>

        <div class="form-title">ID:
            <input class="form-field" type="text" placeholder="4" name="id"/><br /></div>

        <div class="submit-container">
            <input class="submit-button" type="reset"  value = "Reset"/>
            <input class="submit-button" id="blockSubmit" value="Delete student" type="submit" name="cmd"/>
        </div>
    </form>
</div>
<script src="js/main.js"></script>
<%--
<c:out value="${msg}"/>
<ul>
    <c:forEach items="${resultList}" var="outputString">
        <li><c:out value="${outputString}"/></li>
    </c:forEach>
</ul>--%>

<div class="MyTable">
    <table>
        <tr>
            <td>ID</td>
            <td >Name</td>
            <td>Groups number</td>
            <td>Average score</td>
        </tr>

        <c:forEach var="student" items="${resultList}">
            <tr>
                <td><c:out value="${student.id}" /></td>
                <td><c:out value="${student.name}" /></td>
                <td><c:out value="${student.groupsNumber}" /></td>
                <td><c:out value="${student.averScore}" /></td>
            </tr>
        </c:forEach>

    </table>
</div>

</body>
</html>
