<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <link type="text/css" rel="stylesheet" href="css/style.css"/>
    <link type="text/css" rel="stylesheet" href="css/table.css"/>
    <title>Students Control</title>
</head>

<body>

<div class="buttonDiv">
    <div>
        <form action="${pageContext.request.contextPath}\Control" method="get">
            <input type="hidden" value="parse" name="command"/>
            <input class="btn" type="submit" value="Parse DOM" name="btn"/>
            <input class="btn" type = "submit" value = "Parse SAX"  name="btn"/>
            <input class="btn" type = "submit" value = "Parse StAX" name="btn"/>
        </form>
    </div>
</div>


<div class="MyTable">
    <table>
        <tr>
            <td>ID</td>
            <td >Name</td>
            <td>Operator Name</td>
            <td>Payroll</td>
            <td>Call cost inside network</td>
            <td>Call cost outside network</td>
            <td>Call cost to city phone</td>
            <td>Favorite number</td>
            <td>Tariffication</td>
            <td>Connection Fee</td>
        </tr>

        <c:forEach var="tariffType" items="${tariffList}">
            <tr>
                <td><c:out value="${tariffType.id}" /></td>
                <td><c:out value="${tariffType.name}" /></td>
                <td><c:out value="${tariffType.operatorName}" /></td>
                <td><c:out value="${tariffType.payroll.value}" /></td>
                <td><c:out value="${tariffType.callPrices.insideNetwork}" /></td>
                <td><c:out value="${tariffType.callPrices.outsideNetwork}" /></td>
                <td><c:out value="${tariffType.callPrices.cityPhone}" /></td>
                <td><c:out value="${tariffType.parameters.favoriteNumber}" /></td>
                <td><c:out value="${tariffType.parameters.tariffication}" /></td>
                <td><c:out value="${tariffType.parameters.connectionFee}" /></td>
            </tr>
        </c:forEach>

    </table>
</div>

</body>
</html>
