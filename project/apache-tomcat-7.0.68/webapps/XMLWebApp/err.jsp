<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error occurred</title>
    <link type="text/css" rel="stylesheet" href="css/page.css"/>
</head>
<body>
  <h2 align="center">Some error has occurred</h2>
  <p>(For more info check tomcat log file)</p>
  <a href="index.jsp">Go to main page</a>
</body>
</html>
