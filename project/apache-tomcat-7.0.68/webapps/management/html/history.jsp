<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/form.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/info-table.css">
</head>
<title>История</title>
<body>
<div id="header">
    <div id="logo-wrap"><img src="${pageContext.request.contextPath}/img/logo.png"></div>
</div>
<form id="otp-form">
    <div id="wrap-form">
        <h2>История</h2>

        <div class="myTable">
            <table>
                <tr>
                    <td>Номер выплаты</td>
                    <td>Вид</td>
                    <td>ФИО получателя</td>
                    <td>Сумма</td>
                    <td>Дата</td>
                </tr>
                <c:forEach var="salary" items="${salaryList}">
                    <tr>
                        <td><c:out value="${salary.id}"/></td>
                        <td>Зарплата</td>
                        <td><c:out value="${salary.employee.name}"/></td>
                        <td><c:out value="${salary.sum}"/></td>
                        <td><c:out value="${salary.date}"/></td>
                    </tr>
                </c:forEach>
                <c:forEach var="prepayment" items="${prepaymentsList}">
                    <tr>
                        <td><c:out value="${prepayment.id}"/></td>
                        <td>Аванс</td>
                        <td><c:out value="${prepayment.employee.name}"/></td>
                        <td><c:out value="${prepayment.sum}"/></td>
                        <td><c:out value="${prepayment.date}"/></td>
                    </tr>
                </c:forEach>
                <c:forEach var="vacation" items="${vacationList}">
                    <tr>
                        <td><c:out value="${vacation.id}"/></td>
                        <td>Отпускные</td>
                        <td><c:out value="${vacation.employee.name}"/></td>
                        <td><c:out value="${vacation.sum}"/></td>
                        <td><c:out value="${vacation.date}"/></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
        Фильтр:
        <div>
            <form method="get" action="${pageContext.request.contextPath}/Control">
                <input type="hidden" name="cmd" value="selectPrepayments">
            </form>
            <form method="get" action="${pageContext.request.contextPath}/Control">
                <input type="submit" class="control-button" value="Авансы"/>
                <input type="hidden" name="cmd" value="selectPrepayments">
            </form>
            <form method="get" action="${pageContext.request.contextPath}/Control">
                <input type="submit" class="control-button" value="ЗП"/>
                <input type="hidden" name="cmd" value="selectSalary">
            </form>
            <form method="get" action="${pageContext.request.contextPath}/Control">
                <input type="submit" class="control-button" value="Отпускные"/>
                <input type="hidden" name="cmd" value="selectVacations">
            </form>
            <form method="get" action="${pageContext.request.contextPath}/Control">
                <input type="submit" class="control-button" value="Снять"/>
                <input type="hidden" name="cmd" value="selectAllPayments">
            </form>
        </div>
    </div>
</form>

<div class="spacer1"></div>
<div class="footer">
    <form method="get" action="${pageContext.request.contextPath}/Control">
        <input type="submit" class="control-button" value="На главную"/>
        <input type="hidden" name="cmd" value="redirectToMainPage">
    </form>
    <form method="get" action="${pageContext.request.contextPath}/Control">
        <input type="submit" class="control-button" value="Выход"/>
        <input type="hidden" name="cmd" value="logOut">
    </form>
</div>
</body>
</html>