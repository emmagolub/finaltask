<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/form.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/updateEmpl.js"></script>
</head>
<title>Обновление информации о сотруднике</title>
<body>
<div id="header">
    <div id="logo-wrap"><img src="${pageContext.request.contextPath}/img/logo.png"></div>
</div>
<form id="otp-form">

    <div id="wrap-form">

        <h2>Редактирование информации о сотруднике</h2>

        <h4>Введите  табельный номер и нажмите на кнопку "Показать". После редактирования нажмите кнопку "Обновить".</h4>

        <form method="get" action="${pageContext.request.contextPath}/Control">
            <input type="submit" class="control-button" value="Показать"/>
            <input type="hidden" name="cmd" value="employeeUpdateInfo">
            <input type="hidden" name="ID" id="ID">
        </form>

        <form method="post" action="${pageContext.request.contextPath}/Control">
            <table>
                <tr>
                    <td>Табельный номер:</td>

                    <td>
                        <input type="text" class="input" id="number" name="number" value="${number} " required="true"/>
                    </td>

                </tr>
                <tr>
                    <td>ФИО:</td>
                    <td><input type="text" class="input" name="name" value="${name}" /></td>
                </tr>
                <tr>
                    <td>Отдел:</td>
                    <td><input type="text" class="input" name="department" value="${department}"/></td>
                </tr>
                <tr>
                    <td>Должность:</td>
                    <td><input type="text" class="input" name="occupation" value="${occupation}" /></td>
                </tr>
                <tr>
                    <td>Дата поступления:</td>
                    <td><input type="date" class="input" name="date" value="${date}" /></td>
                </tr>
                <tr>
                    <td>Количество детей:</td>
                    <td><input type="number" class="input" name="numberOfChildren" value="${numberOfChildren}" /></td>
                </tr>

                <tr>
                    <td>Одинокий родитель:</td>
                    <td><input type="checkbox" class="input" name="singleParent"id="singleParent" value="${singleParent}"/></td>
                </tr>
                <tr>
                    <td>Пострадавший от ЧАЭС:</td>
                    <td><input type="checkbox" class="input" name="cnpp" id = "cnpp" value="${cnpp}"/></td>
                </tr>
            </table>
            <p id="message">${message}</p>
            <input type="submit" class="control-button" value="Обновить"/>
            <input type="hidden" name="cmd" value="updateEmployee">
        </form>

    </div>
</form>

<div class="spacer1"></div>
<div class="footer">
<form method="get" action="${pageContext.request.contextPath}/Control">
    <input type="submit" class="control-button" value="На главную"/>
    <input type="hidden" name="cmd" value="redirectToMainPage">
</form>

<form method="get" action="${pageContext.request.contextPath}/Control">
    <input type="submit" class="control-button" value="Выход"/>
    <input type="hidden" name="cmd" value="logOut">
</form>
</div>


</body>

</html>