<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/form.css"/>
</head>
<title>Удалить сотрудника</title>
<body>
<div id="header">
    <div id="logo-wrap"><img src="${pageContext.request.contextPath}/img/logo.png"></div>
</div>
<form id="otp-form" action="${pageContext.request.contextPath}/Control" method="post">
    <div id="wrap-form">
        <h2>Увольнение сотрудника</h2>
        <h4>Полностью заполните форму и нажмите "Удалить".</h4>
        <table>
            <tr>
                <td>Табельный номер сотрудника:</td>
                <td><input type="text" pattern="^\d+$" class="input" required="true" name="personalNumber"/></td>
            </tr>
            <tr>
                <td>Дата увольнения:</td>
                <td><input type="date" class="input" name="dismissingDate" required="true"/></td>
            </tr>
        </table>
        <input type="submit" class="control-button" value="Удалить"/>
        <input type="hidden" name="cmd" value="dismissEmployee">
    </div>
    <p id="message">${message}</p>
</form>
<div class="spacer1"></div>
<div class="footer">
    <form method="get" action="${pageContext.request.contextPath}/Control">
        <input type="submit" class="control-button" value="На главную"/>
        <input type="hidden" name="cmd" value="redirectToMainPage">
    </form>
    <form method="get" action="${pageContext.request.contextPath}/Control">
        <input type="submit" class="control-button" value="Выход"/>
        <input type="hidden" name="cmd" value="logOut">
    </form>
</div>
</body>
</html>