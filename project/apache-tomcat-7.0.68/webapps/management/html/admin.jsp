<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/form.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/info-table.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/admin.css"/>
</head>
<title>Администратор</title>
<body>
<div id="header">
    <div id="logo-wrap"><img src="${pageContext.request.contextPath}/img/logo.png"></div>
</div>
<div class="spacer1"></div>
<div id="center-block" class="myTable">
    <form id="add-user-form" method="post" action="${pageContext.request.contextPath}/Control">
        Логин: <input type="text" class="input" name="loginToAdd"/>
        Пароль: <input type="password" class="input" name="password"/>
        <input type="submit" class="control-button" value="Добавить"/>
        <input type="hidden" name="cmd" value="addUser">
    </form>
    <form id="lock-user-form" method="post" action="${pageContext.request.contextPath}/Control">
        Логин: <input type="text" class="input" name="loginToLock"/>
        <input type="submit" class="control-button" value="Заблокировать"/>
        <input type="hidden" name="cmd" value="lockUser">
    </form>
    <form id="unlock-user-form" method="post" action="${pageContext.request.contextPath}/Control">
        Логин: <input type="text" class="input" name="loginToUnlock"/>
        <input type="submit" class="control-button" value="Разблокировать"/>
        <input type="hidden" name="cmd" value="unlockUser">
    </form>
</div>
<form id="otp-form">
    <div id="wrap-form">
        <h4>Пользователи</h4>
        <p id="message">${message}</p>
        <div class="myTable">
            <table>
                <tr>
                    <td>Логин</td>
                    <td>Роль</td>
                    <td>Статус</td>
                </tr>
                <c:forEach var="account" items="${accountList}">
                    <tr>
                        <td><c:out value="${account.login}"/></td>
                        <td><c:out value="${account.role}"/></td>
                        <td><c:out value="${account.status}"/></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
</form>
<div class="footer">
    <form method="get" action="${pageContext.request.contextPath}/Control">
        <input type="submit" class="control-button" value="Выход"/>
        <input type="hidden" name="cmd" value="logOut">
    </form>
</div>
</body>
</html>