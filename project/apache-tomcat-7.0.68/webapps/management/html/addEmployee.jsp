<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setBundle basename="${bundle}" var="lang"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/form.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<title><fmt:message key="title.add.employee" bundle="${lang}"/></title>
<body>

<div id="header">
    <div id="logo-wrap"><img src="${pageContext.request.contextPath}/img/logo.png"></div>
</div>
<div id="wrap-form">
    <div align="rigth">
        <form name="changeLangForm" action="Control" method="get">
            <input type="hidden" name="cmd" value="changeLang">
            <input type="hidden" name="path" value="login.jsp">
            <input type="submit" name="langBtn" value="ru">
            <input type="submit" name="langBtn" value="en">
        </form>
    </div>
    <h2><fmt:message key="h2.add.employee" bundle="${lang}"/></h2>
    <h4><fmt:message key="h4.description.add.employee" bundle="${lang}"/></h4>
    <form method="post" action="${pageContext.request.contextPath}/Control">
        <table>
            <tr>
                <td><fmt:message key="td.name" bundle="${lang}"/></td>
                <td><input type="text" class="input" name="name" required="true"/></td>
            </tr>
            <tr>
                <td><fmt:message key="td.department" bundle="${lang}"/></td>
                <td><input type="text" class="input" name="department" required="true"/></td>
            </tr>
            <tr>
                <td><fmt:message key="td.occupation" bundle="${lang}"/></td>
                <td><input type="text" class="input" name="occupation" required="true"/></td>
            </tr>
            <tr>
                <td><fmt:message key="td.start.date" bundle="${lang}"/></td>
                <td><input type="date" class="input" name="date" required="true"/></td>
            </tr>
            <tr>
                <td><fmt:message key="td.children.number" bundle="${lang}"/> детей:</td>
                <td><input type="number" class="input" name="numberOfChildren" required="true"/></td>
            </tr>

            <tr>
                <td><fmt:message key="td.single.parent" bundle="${lang}"/></td>
                <td><input type="checkbox" class="input" name="singleParent" value="singleParent"/></td>
            </tr>
            <tr>
                <td><fmt:message key="td.cnpp" bundle="${lang}"/></td>
                <td><input type="checkbox" class="input" name="cnpp" value="cnpp"/></td>
            </tr>
        </table>
        <input type="submit" class="control-button" value="<fmt:message key="button.add" bundle="${lang}"/>"/>
        <input type="hidden" name="cmd" value="addEmployeeCmd">
    </form>
    <p id="message">${message}</p>
</div>
<div class="spacer1"></div>

<div class="footer">
    <form method="get" action="${pageContext.request.contextPath}/Control">
        <input type="submit" class="control-button" value="<fmt:message key="button.main.page" bundle="${lang}"/>"/>
        <input type="hidden" name="cmd" value="redirectToMainPage">
    </form>
    <form method="get" action="${pageContext.request.contextPath}/Control">
        <input type="submit" class="control-button" value="<fmt:message key="button.logout" bundle="${lang}"/>"/>
        <input type="hidden" name="cmd" value="logOut">
    </form></div>
</body>
</html>