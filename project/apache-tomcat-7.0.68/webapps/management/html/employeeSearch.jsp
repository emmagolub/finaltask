<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/form.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/info-table.css">
</head>
<title>Поиск по сотруднику</title>
<body>
<div id="header">
    <div id="logo-wrap"><img src="${pageContext.request.contextPath}/img/logo.png"></div>
</div>
<form id="otp-form" action="${pageContext.request.contextPath}/Control" method="get">
    <div id="wrap-form">
        <h2>Поиск по сотруднику</h2>
        <h4>Введите личный номер сотрудника и нажмите "Искать"</h4>
        Номер: <input type="text" pattern="^\d+$" class="input" required="true" name="number"/>
        <input type="submit" class="control-button" value="Искать"/>
        <input type="hidden" name="cmd" value="employeeSearch">
        <h2></h2>
        <div class="myTable">
            <table>
                <tr>
                    <td>Выплата</td>
                    <td>Дата</td>
                    <td>Сумма</td>
                </tr>
                <c:forEach var="salary" items="${salaryList}">
                    <tr>
                        <td>Зарплата</td>
                        <td><c:out value="${salary.date}"/></td>
                        <td><c:out value="${salary.sum}"/></td>
                    </tr>
                </c:forEach>
                <c:forEach var="prepayment" items="${prepaymentsList}">
                    <tr>
                        <td>Аванс</td>
                        <td><c:out value="${prepayment.date}"/></td>
                        <td><c:out value="${prepayment.sum}"/></td>
                    </tr>
                </c:forEach>
                <c:forEach var="vacation" items="${vacationList}">
                    <tr>
                        <td>Отпускные</td>
                        <td><c:out value="${vacation.date}"/></td>
                        <td><c:out value="${vacation.sum}"/></td>
                    </tr>
                </c:forEach>
            </table>
        </div>

    </div>
</form>



<div class="spacer1"></div>
<div class="footer">
    <form method="get" action="${pageContext.request.contextPath}/Control">
        <input type="submit" class="control-button" value="На главную"/>
        <input type="hidden" name="cmd" value="redirectToMainPage">
    </form>
    <form method="get" action="${pageContext.request.contextPath}/Control">
        <input type="submit" class="control-button" value="Выход"/>
        <input type="hidden" name="cmd" value="logOut">
    </form>
</div>
</body>
</html>