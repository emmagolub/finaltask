<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/form.css"/>
</head>
<title>Начисление отпускных</title>

<body>
<div id="header">
    <div id="logo-wrap"><img src="${pageContext.request.contextPath}/img/logo.png"></div>
</div>
<form id="otp-form" method="post" action="${pageContext.request.contextPath}/Control">
<div id="wrap-form">
    <h2>Начисление отпускных</h2>
    <h4>Полностью заполните форму и нажмите кнопку "Начислить".</h4>
    <table >
      <tr>
        <td >Табельный номер сотрудника:</td>
        <td ><input type="text"  pattern="^\d+$" class="input" required="true" name="number"/></td>
      </tr>
      <tr>
        <td >Сумма к начислению:</td>
        <td ><input type="text"  pattern="^\d+$" class="input" required="true" name = "sum"/></td>
      </tr>
      <tr>
        <td >Дата начисления:</td>
        <td > <input type="date"   class="input" required="true" name="date"/></td>
      </tr>
      <tr>
        <td >Начало отпуска:</td>
        <td ><input type="date"   class="input" required="true" name="startDate"/></td>
      </tr>
      <tr>
      <td >Конец отпуска:</td>
        <td ><input type="date"   class="input" required="true" name="endDate"/></td>
      </tr>
    </table>
    <input type="submit" class="control-button" value="Начислить"/>
    <input type="hidden" name="cmd" value="addVacation">
</div>
</form>
<div class="spacer1"></div>
<div class="footer">
    <form method="get" action="${pageContext.request.contextPath}/Control">
        <input type="submit" class="control-button" value="На главную"/>
        <input type="hidden" name="cmd" value="redirectToMainPage">
    </form>
    <form method="get" action="${pageContext.request.contextPath}/Control">
        <input type="submit" class="control-button" value="Выход"/>
        <input type="hidden" name="cmd" value="logOut">
    </form></div>
</body>
</html>