<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link type="text/css" rel="stylesheet"  href="${pageContext.request.contextPath}/css/form.css"/>
  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/info-table.css">
</head>
<title>Архив работников</title>
<body>

<div id="header">
    <div id="logo-wrap"><img src="${pageContext.request.contextPath}/img/logo.png"></div>
</div>
	

<form id="otp-form" >

<div id="wrap-form">

    <h4>Уволенные сотрудники</h4>
  
    <div class="myTable" >
                <table >
                    <tr>
                        <td> Номер </td>
                        <td > ФИО </td>
                        <td>Отдел</td>
                        <td>Должность</td>
                        <td >Дата увольнения</td>
                        <td>Дата поступления</td>
                    </tr>
                    <c:forEach var="employee" items="${dismissedList}">
                        <tr>
                            <td><c:out value="${employee.number}" /></td>
                            <td><c:out value="${employee.name}" /></td>
                            <td><c:out value="${employee.department}" /></td>
                            <td><c:out value="${employee.occupation}" /></td>
                            <td><c:out value="${employee.startDate}" /></td>
                            <td><c:out value="${employee.finishDate}" /></td>
                        </tr>
                    </c:forEach>
                    
                </table>
            </div>
</div>
</form>
<div class="spacer1"></div>

<div class="footer">Copyright in footer.
    <form method="get" action="${pageContext.request.contextPath}/Control">
        <input type="submit" class="control-button" value="На главную"/>
        <input type="hidden" name="cmd" value="redirectToMainPage">
    </form>
    <form method="get" action="${pageContext.request.contextPath}/Control">
        <input type="submit" class="control-button" value="Выход"/>
        <input type="hidden" name="cmd" value="logOut">
    </form>
</div>


</body>

</html>