<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="../css/main.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css"/>
</head>
<title>Главная</title>
<body>

<div id="header">
    <div id="logo-wrap"><img src="${pageContext.request.contextPath}/img/logo.png"></div>
</div>

<div class="wrap-table">
    <table>
        <tr>
            <td>
                <form method="get" action="${pageContext.request.contextPath}/Control">
                    <input type="submit" class="control-button" value="Просмотр сотрудников"/>
                    <input type="hidden" name="cmd" value="showEmployee">
                </form>
            </td>
            <td>
                <form method="get" action="${pageContext.request.contextPath}/Control">
                    <input type="submit" class="control-button" value="История"/>
                    <input type="hidden" name="cmd" value="selectAllPayments">
                </form>
            </td>
        </tr>
        <tr>
            <td>
                <form method="get" action="${pageContext.request.contextPath}/Control">
                    <input type="submit" class="control-button" value="Начисление ЗП"/>
                    <input type="hidden" name="cmd" value="redirectToAddSalary">
                </form>
            </td>
            <td>
                <form method="get" action="${pageContext.request.contextPath}/Control">
                    <input type="submit" class="control-button" value="Поиск по работнику"/>
                    <input type="hidden" name="cmd" value="redirectToEmployeeSearch">
                </form>
            </td>
        </tr>
        <tr>
            <td>
                <form method="get" action="${pageContext.request.contextPath}/Control">
                    <input type="submit" class="control-button" value="Начисление аванса"/>
                    <input type="hidden" name="cmd" value="redirectToAddPrepayment">
                </form>
            </td>
            <td>
                <form method="get" action="${pageContext.request.contextPath}/Control">
                    <input type="submit" class="control-button" value="Удалить сотрудника"/>
                    <input type="hidden" name="cmd" value="redirectToRemoveEmployee">
                </form>
            </td>
        </tr>
        <tr>
            <td>
                <form method="get" action="${pageContext.request.contextPath}/Control">
                    <input type="submit" class="control-button" value="Начисление отпускных"/>
                    <input type="hidden" name="cmd" value="redirectToAddVacation">
                </form>
            </td>
            <td>
                <form method="get" action="${pageContext.request.contextPath}/Control">
                    <input type="submit" class="control-button" value="Добавление сотрудника"/>
                    <input type="hidden" name="cmd" value="redirectToAddEmployee">
                </form>
            </td>
        </tr>
        <tr>
            <td>
                <form method="get" action="${pageContext.request.contextPath}/Control">
                    <input type="submit" class="control-button" value="Обновление инф. о сотруднике"/>
                    <input type="hidden" name="cmd" value="redirectToUpdateEmployee">
                </form>
            </td>
            <td>
                <form method="get" action="${pageContext.request.contextPath}/Control">
                    <input type="submit" class="control-button" value="Уволенные сотрудники"/>
                    <input type="hidden" name="cmd" value="showDismissed">
                </form>
            </td>
    </table>

</div>
<div class="spacer1"></div>

<div class="footer">Copyright in footer.
    <form method="get" action="${pageContext.request.contextPath}/Control">
        <input type="submit" class="control-button" value="Выход"/>
        <input type="hidden" name="cmd" value="logOut">
    </form></div>


</body>

</html>