<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/form.css"/>
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/updateEmpl.js"></script>
</head>
<title>Добавление зарплаты</title>
<body>

<div id="header">
    <div id="logo-wrap"><img src="${pageContext.request.contextPath}/img/logo.png"></div>
</div>

<div id="wrap-form">
    <h2>Начисление заработной платы</h2>
    <h4>Введите табельный номер и нажмите на кнопку "Показать". После полного заполнения формы нажмите
        "Начислить".</h4>
    <form method="get" action="${pageContext.request.contextPath}/Control">
        <input type="submit" class="control-button" value="Показать"/>
        <input type="hidden" name="cmd" value="addSalaryInfo">
        <input type="hidden" name="ID" id="ID">
    </form>
    <form id="nzp-form" method="post" action="${pageContext.request.contextPath}/Control">

        <p>
            Личный номер: <input type="text" pattern="^\d+$" class="input" required="true" value="${number}"
                                 id="number" name="number"/>
            ФИО: <input type="text"  class="input" value="${name}"/>
            Отдел: <input type="text"  class="input" value="${department}"/>
            Должность: <input type="text" class="input" value="${occupation}"/>
        </p>
        <table>
            <tr>
                <td><h4>Начисление</h4></td>
            </tr>
            <tr>
                <td>Оплата по окладу:</td>
                <td><input type="text" pattern="^\d+$" class="input" name ="salary"/></td>
            </tr>
            <tr>
                <td>Оплата по часам:</td>
            </tr>
            <tr>
                <td>Количество часов:</td>
                <td><input type="text" pattern="^\d+$" class="input" name="hoursNumber"/></td>
            </tr>
            <tr>
                <td>Сумма за час:</td>
                <td><input type="text" pattern="^\d+$" class="input" name="hourPayment"/></td>
            </tr>
            <tr>
                <td>Доплата за выходные и праздники:</td>
                <td><input type="text" pattern="^\d+$" class="input" name="vocationSurcharge"/></td>
            </tr>
            <tr>
                <td>Доплата за переработку:</td>
                <td><input type="text" pattern="^\d+$" class="input" name="overLoadSurcharge"/></td>
            </tr>
            <tr>
                <td>Премия:</td>
                <td><input type="text" pattern="^\d+$" class="input" name="bonus"/></td>
            </tr>
            <tr>
                <td><h4>Удержание</h4></td>
            </tr>
            <tr>
                <td>Штрафы:</td>
                <td><input type="text" pattern="^\d+$" class="input" name="fine"/></td>
            </tr>
            <tr>
                <td>Аванс:</td>
                <td><input type="text" pattern="^\d+$" class="input" name="prepayment"/></td>
            </tr>
            <tr>
                <td>Профвзносы:</td>
                <td><input type="text" pattern="^\d+$" class="input" name="profContribution"/></td>
            </tr>
            <tr>
                <td>Дата начисления:</td>
                <td><input type="date" class="input" name="date"/></td>
            </tr>
        </table>
        <input type="submit" class="control-button" value="Начислить"/>
        <input type="hidden" name="cmd" value="addSalary">
    </form>
</div>

<div class="spacer1"></div>
<div class="footer">
    <form method="get" action="${pageContext.request.contextPath}/Control">
    <input type="submit" class="control-button" value="На главную"/>
    <input type="hidden" name="cmd" value="redirectToMainPage">
</form>
    <form method="get" action="${pageContext.request.contextPath}/Control">
        <input type="submit" class="control-button" value="Выход"/>
        <input type="hidden" name="cmd" value="logOut">
    </form>
</div>
</body>
</html>