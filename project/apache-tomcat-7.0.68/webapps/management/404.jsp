<!DOCTYPE HTML>
<html>
	<head>
		<title>0hh Website Template | Home :: W3layouts</title>
		<link href="${pageContext.request.contextPath}/css/404.css" rel="stylesheet" type="text/css"  media="all" />
	</head>
	<body>
		<div class="wrap">
				<div class="header">
					<div class="logo">
						<h1><a href="#">Ohh</a></h1>
					</div>
				</div>
			<div class="content">
				<img src="${pageContext.request.contextPath}/img/error-img.png" title="error" />
				<p><span><label>O</label>hh.....</span>You Requested the page that is no longer There.</p>
                <a href="${pageContext.request.contextPath}/html/main.jsp">Back To Home</a>
				<div class="copy-right">
					<p>&#169 All rights Reserved</p>
				</div>
   			</div>
		</div>
	</body>
</html>

