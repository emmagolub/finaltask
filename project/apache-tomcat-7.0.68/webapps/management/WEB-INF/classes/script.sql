CREATE SCHEMA IF NOT EXISTS`educationCenter` 
DEFAULT CHARACTER SET utf8 ;

CREATE TABLE IF NOT EXISTS `educationCenter`.`Employee`
( 
    `Number` INT NOT NULL AUTO_INCREMENT ,
    `Name` VARCHAR (100), 
    `Date` DATE, 
    `department` VARCHAR(45) NULL , 
    `occupation` VARCHAR (45) NULL, 
    `NumberOfChildren` INT,
    `SingleParent` BOOL,
    `CNPP` BOOL,
    PRIMARY KEY (`Number`) 
); 

CREATE TABLE IF NOT EXISTS `educationCenter`.`Salary`
( 
    `ID` INT NOT NULL AUTO_INCREMENT ,
	`EmployeeNumber` INT NOT NULL, 
    `Date` DATE NULL, 
    `Sum` INT NULL , 
    PRIMARY KEY (`ID`), 
    FOREIGN KEY (EmployeeNumber)
      REFERENCES Employee(Number)
      ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `educationCenter`.`Prepayment`
( 
    `ID` INT NOT NULL AUTO_INCREMENT ,
	`EmployeeNumber` INT NOT NULL, 
    `Date` DATE NULL, 
    `Sum` INT NULL , 
    PRIMARY KEY (`ID`), 
    FOREIGN KEY (EmployeeNumber)
      REFERENCES Employee(Number)
      ON DELETE CASCADE
) ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS `educationCenter`.`Vacation`
( 
    `ID` INT NOT NULL AUTO_INCREMENT ,
	`EmployeeNumber` INT NOT NULL, 
    `Date` DATE NULL, 
    `Sum` INT NULL , 
    `Period` VARCHAR (40),
    PRIMARY KEY (`ID`), 
    FOREIGN KEY (EmployeeNumber)
      REFERENCES Employee(Number)
      ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `educationCenter`.`Authorization`
( 
    `Login` VARCHAR (50),
	`Password` VARCHAR (300), 
    `Role` BOOL,
	`Status` BOOL,
    PRIMARY KEY (`Login`)
); 

CREATE TABLE IF NOT EXISTS `educationCenter`.`Dismissed`
( 
    `Number` INT NOT NULL AUTO_INCREMENT,
	`Name` VARCHAR (100), 
    `StartDate` DATE, 
    `FinishDate` DATE, 
    `department` VARCHAR(45) NULL , 
    `occupation` VARCHAR (45) NULL,
    PRIMARY KEY (`Number`)    
); 

/*CREATE TABLE IF NOT EXISTS `educationCenter`.`Company`
( 
    `Name` VARCHAR(45) NOT NULL,
    `Adress` VARCHAR(100) NOT NULL, 
    `Telephone` VARCHAR(45) NOT NULL, 
    `CEO` VARCHAR(100) NOT NULL , 
    `ChiefAccountant` VARCHAR(100) NOT NULL,
    `INN` VARCHAR(45) NOT NULL, 
    `Bank` VARCHAR(100) NOT NULL ,
    `CheckingAccount` VARCHAR(45) NOT NULL
); */

CREATE TABLE IF NOT EXISTS `educationCenter`.`IncomeTax`
( 
	`RecordID` INT NOT NULL AUTO_INCREMENT,
	`StandartPercent` INT NOT NULL,
    `MinimumBet` INT NOT NULL,
    `MinimumAmount` INT NOT NULL, 
    `AmountPerChild` INT NOT NULL, 
    `AmountForSingleParent` INT NOT NULL , 
    `AmmountForAffectedByCNPP` INT NOT NULL,
    `PensionFund` INT NOT NULL,
     PRIMARY KEY (`RecordID`)
); 
 