package com.epam.passwordmanager;

import junit.framework.TestCase;

/**
 * @author Emma
 */
public class PasswordManagerTests extends TestCase {

    private PasswordManager passwordManager = PasswordManager.getInstance();

    public void testEqualTruePassword() throws Exception {
        String incomingTruePassword = "12322";
        String realPassword = "5zspy648578f040c8d32be3ba06cdbed41e97";
        assertTrue(passwordManager.comparePasswords(incomingTruePassword, realPassword));
    }

    public void testEqualFalsePassword() throws Exception {
        String incomingFalsePassword = "qwe";
        String realPassword = "5zspy648578f040c8d32be3ba06cdbed41e97";
        assertFalse(passwordManager.comparePasswords(incomingFalsePassword, realPassword));

    }

    public void testEqualEmptyPassword() throws Exception {
        String incomingEmptyPassword = "";
        String realPassword = "5zspy648578f040c8d32be3ba06cdbed41e97";
        assertFalse(passwordManager.comparePasswords(incomingEmptyPassword, realPassword));
    }
}