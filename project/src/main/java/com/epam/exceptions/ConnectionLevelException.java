package com.epam.exceptions;

/**
 * @author Эмма
 */
public class ConnectionLevelException extends Exception{
    public ConnectionLevelException(String message){
        super(message);
    }

    public ConnectionLevelException(String message, Exception e){
        super(message, e);
    }
}
