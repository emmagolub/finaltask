package com.epam.control;

import com.epam.logic.command.CommandManager;
import com.epam.constants.ButtonsCommands;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This servlet is Controller of MVC pattern. It controls,
 * do or post called, and to which view forward/redirect
 * @author Emma
 */
public class ControlServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pathToRedirect = process(request);
        request.getRequestDispatcher(pathToRedirect).forward(request, response);
    }

    /**
     * Calling Post method causes redirecting to <code>DisplayServlet</code>. This is
     * main part of PRG pattern.
     * @param request httpServletRequest
     * @param response httpServletResponse
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String pathToRedirect = process(request);
        response.sendRedirect("Display?page=" + pathToRedirect);
    }

    /**
     * This method defines which command should be executed
     * @param request httpServletRequest
     * @return Page alias of result view page
     * @throws IOException
     */
    private String process(HttpServletRequest request) throws IOException{
        String command = request.getParameter(ButtonsCommands.COMMAND);
        CommandManager commandManager = CommandManager.getInstance();
        return commandManager.getCommand(command).execute(request);
    }
}
