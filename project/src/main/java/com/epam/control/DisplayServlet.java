package com.epam.control;

import com.epam.constants.Attributes;
import com.epam.constants.PagePathes;
import com.epam.util.AccessFilter;
import com.epam.util.ContentManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * This servlet is Display of Post Redirect Get pattern. It helps to make
 * post requests calls idempotent - i.e. they can be executed only once. It acts in pair
 * with <code>ControlServlet</code> and just show result page after operation.
 * @author Emma
 */
public class DisplayServlet extends HttpServlet{
    private static final long serialVersionUID = 1L;

    public DisplayServlet() {
        super();
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher(PagePathes.PATH_TO_ERROR_PAGE).forward(request, response);
    }

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        String location = request.getParameter(Attributes.PAGE);
        if (location == null){
            location = request.getSession().getAttribute(Attributes.PAGE).toString();
        }
        ContentManager.addContent(location, request);
        if (location != null && AccessFilter.isRoleTrue(request, location)) {
            request.getRequestDispatcher(location).forward(request, response);
        }
        else {
            request.getRequestDispatcher(PagePathes.PATH_TO_ERROR_PAGE).forward(request, response);
        }
    }
}
