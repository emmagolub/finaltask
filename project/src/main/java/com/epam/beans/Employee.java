package com.epam.beans;

import java.io.Serializable;

/**
 * @author Эмма
 */

public class Employee implements Cloneable, Serializable{
    private String name;
    private String date;
    private String occupation;
    private String department;
    private int numberOfChildren;
    private boolean singleParent;
    private boolean cnpp;
    private int number;

    public Employee(){

    }

    public Employee(boolean cnpp, boolean singleParent, int numberOfChildren, String department, String occupation, String date, String name, int number) {
        this.cnpp = cnpp;
        this.singleParent = singleParent;
        this.numberOfChildren = numberOfChildren;
        this.department = department;
        this.occupation = occupation;

        this.date = date;
        this.name = name;
        this.number = number;
    }

    public Employee(boolean cnpp, boolean singleParent, int numberOfChildren, String department, String occupation, String date, String name) {
        this.cnpp = cnpp;
        this.singleParent = singleParent;
        this.numberOfChildren = numberOfChildren;
        this.department = department;
        this.occupation = occupation;

        this.date = date;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public int getNumberOfChildren() {
        return numberOfChildren;
    }

    public void setNumberOfChildren(int numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

    public boolean isSingleParent() {
        return singleParent;
    }

    public void setSingleParent(boolean singleParent) {
        this.singleParent = singleParent;
    }

    public boolean isCnpp() {
        return cnpp;
    }

    public void setCnpp(boolean cnpp) {
        this.cnpp = cnpp;
    }



    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Employee clone (){
        return new Employee(this.cnpp, this.singleParent, this.numberOfChildren,
                this.department,this.occupation, this.date, this.name, this.number);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", date='" + date + '\'' +
                ", occupation='" + occupation + '\'' +
                ", department='" + department + '\'' +
                ", numberOfChildren=" + numberOfChildren +
                ", singleParent=" + singleParent +
                ", cnpp=" + cnpp +
                ", number=" + number +
                '}';
    }
}
