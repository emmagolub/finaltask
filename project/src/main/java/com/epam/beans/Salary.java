package com.epam.beans;

import java.sql.Date;

/**
 * @author Эмма
 */
public class Salary {
    private Date date;
    private Employee employee;
    private float sum;
    private float salary;
    private int hoursNumber;
    private float hourPayment;
    private float vocationSurcharge;
    private float overLoadSurcharge;
    private float bonus;
    private float fine;
    private float prepayment;
    private float profContribution;
    private int id;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Salary{");
        sb.append("date='").append(date).append('\'');
        sb.append(", employee=").append(employee);
        sb.append(", sum=").append(sum);
        sb.append(", salary=").append(salary);
        sb.append(", hoursNumber=").append(hoursNumber);
        sb.append(", hourPayment=").append(hourPayment);
        sb.append(", vocationSurcharge=").append(vocationSurcharge);
        sb.append(", overLoadSurcharge=").append(overLoadSurcharge);
        sb.append(", bonus=").append(bonus);
        sb.append(", fine=").append(fine);
        sb.append(", prepayment=").append(prepayment);
        sb.append(", profContribution=").append(profContribution);
        sb.append(", id=").append(id);
        sb.append('}');
        return sb.toString();
    }

    public Salary (){}

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public float getProfContribution() {
        return profContribution;
    }

    public void setProfContribution(float profContribution) {
        this.profContribution = profContribution;
    }

    public float getPrepayment() {
        return prepayment;
    }

    public void setPrepayment(float prepayment) {
        this.prepayment = prepayment;
    }

    public float getFine() {
        return fine;
    }

    public void setFine(float fine) {
        this.fine = fine;
    }

    public float getBonus() {
        return bonus;
    }

    public void setBonus(float bonus) {
        this.bonus = bonus;
    }

    public float getOverLoadSurcharge() {
        return overLoadSurcharge;
    }

    public void setOverLoadSurcharge(float overLoadSurcharge) {
        this.overLoadSurcharge = overLoadSurcharge;
    }

    public float getVocationSurcharge() {
        return vocationSurcharge;
    }

    public void setVocationSurcharge(float vocationSurcharge) {
        this.vocationSurcharge = vocationSurcharge;
    }

    public float getHourPayment() {
        return hourPayment;
    }

    public void setHourPayment(float hourPayment) {
        this.hourPayment = hourPayment;
    }

    public int getHoursNumber() {
        return hoursNumber;
    }

    public void setHoursNumber(int hoursNumber) {
        this.hoursNumber = hoursNumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getSum() {
        return sum;
    }

    public void setSum(float sum) {
        this.sum = sum;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
