package com.epam.beans;

/**
 * @author Эмма
 */
public class DismissedEmployee {
    private int number;
    private String name;
    private String startDate;
    private String finishDate;
    private String department;
    private String occupation;

    public DismissedEmployee(){}

    public DismissedEmployee(int number, String occupation, String department, String finishDate, String startDate, String name) {
        this.number = number;
        this.occupation = occupation;
        this.department = department;
        this.finishDate = finishDate;
        this.startDate = startDate;
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    @Override
    public String toString() {
        return "DismissedEmployee{" +
                "number=" + number +
                ", name='" + name + '\'' +
                ", startDate='" + startDate + '\'' +
                ", finishDate='" + finishDate + '\'' +
                ", department='" + department + '\'' +
                ", occupation='" + occupation + '\'' +
                '}';
    }
}
