package com.epam.logic.command.commandImpl;

import com.epam.beans.Employee;
import com.epam.beans.Salary;
import com.epam.logic.command.Command;
import com.epam.constants.Attributes;
import com.epam.constants.Messages;
import com.epam.constants.PagePathes;
import com.epam.dao.daoimpl.SalaryDao;
import com.epam.exceptions.DaoException;
import com.epam.util.SalaryCounter;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;

/**
 * This command performs new salary addition to DB
 *
 * @author Эмма
 */
public class AddSalaryCmd implements Command {
    private static Logger logger = Logger.getLogger(AddSalaryCmd.class);

    @Override
    public String execute(HttpServletRequest request) {
        Employee employee = new Employee();
        Salary salary = new Salary();
        try {
            employee.setNumber(Integer.parseInt(request.getParameter(Attributes.NUMBER)));
            salary.setEmployee(employee);
            salary.setSalary(Float.parseFloat(request.getParameter(Attributes.SALARY)));
            salary.setHourPayment(Float.parseFloat(request.getParameter(Attributes.HOUR_PAYMENT)));
            salary.setHoursNumber(Integer.parseInt(request.getParameter(Attributes.HOURS_NUMBER)));
            salary.setVocationSurcharge(Float.parseFloat(request.getParameter(Attributes.VOCATION_SURCHARGE)));
            salary.setOverLoadSurcharge(Float.parseFloat(request.getParameter(Attributes.OVERLOAD_SURCHARGE)));
            salary.setBonus(Float.parseFloat(request.getParameter(Attributes.BONUS)));
            salary.setFine(Float.parseFloat(request.getParameter(Attributes.FINE)));
            salary.setPrepayment(Float.parseFloat(request.getParameter(Attributes.PREPAYMENT)));
            salary.setProfContribution(Float.parseFloat(request.getParameter(Attributes.PROF_CONTRIBUTION)));
            salary.setDate(Date.valueOf(request.getParameter(Attributes.DATE)));

            SalaryCounter.countSalary(salary);
            SalaryDao.getInstance().insert(salary);
            logger.info(Messages.SALARY_ADDED);
        } catch (DaoException | NumberFormatException e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
            return PagePathes.PATH_TO_ADD_SALARY_PAGE;
        }
        request.getSession().setAttribute(Attributes.PAGE, PagePathes.PATH_TO_SALARIES_HISTORY);
        return PagePathes.PATH_TO_SALARIES_HISTORY;
    }
}
