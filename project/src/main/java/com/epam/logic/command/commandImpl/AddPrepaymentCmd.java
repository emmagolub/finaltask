package com.epam.logic.command.commandImpl;

import com.epam.beans.Employee;
import com.epam.beans.Prepayment;
import com.epam.logic.command.Command;
import com.epam.constants.Attributes;
import com.epam.constants.Messages;
import com.epam.constants.PagePathes;
import com.epam.dao.daoimpl.PrepaymentDao;
import com.epam.exceptions.DaoException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;

/**
 * This command performs new prepayment addition to DB
 *
 * @author Эмма
 */
public class AddPrepaymentCmd implements Command {
    private static Logger logger = Logger.getLogger(AddPrepaymentCmd.class);

    @Override
    public String execute(HttpServletRequest request) {
        Employee employee = new Employee();
        Prepayment prepayment = new Prepayment();
        try {
            employee.setNumber(Integer.parseInt(request.getParameter(Attributes.NUMBER)));
            prepayment.setEmployee(employee);
            prepayment.setSum(Integer.parseInt(request.getParameter(Attributes.SUM)));
            prepayment.setDate(Date.valueOf(request.getParameter(Attributes.DATE)));
            PrepaymentDao.getInstance().insert(prepayment);
            logger.info(Messages.PREPAYMENT_ADDED);
        } catch (DaoException | NumberFormatException e) {
            logger.error(e.getMessage(), e);
            return PagePathes.PATH_TO_ADD_PREPAYMENT_PAGE;
        }
        request.getSession().setAttribute(Attributes.PAGE, PagePathes.PATH_TO_PREPAYMENTS_HISTORY);
        return PagePathes.PATH_TO_PREPAYMENTS_HISTORY;
    }
}
