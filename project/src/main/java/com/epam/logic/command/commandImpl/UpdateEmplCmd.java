package com.epam.logic.command.commandImpl;

import com.epam.beans.Employee;
import com.epam.logic.command.Command;
import com.epam.constants.Attributes;
import com.epam.constants.Messages;
import com.epam.constants.PagePathes;
import com.epam.dao.daoimpl.EmployeeDao;
import com.epam.exceptions.DaoException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Татьяна on 31.03.2016.
 */
public class UpdateEmplCmd implements Command {
    private static Logger logger = Logger.getLogger(UpdateEmplCmd.class);

    @Override
    public String execute(HttpServletRequest request) {
        Boolean cnpp = false, singleParent = false;

        try {

            if (request.getParameter(Attributes.CNPP) != null) {
                cnpp = true;
            }
            if (request.getParameter(Attributes.SINGLE_PARENT) != null){
                singleParent = true;
            }
            int childrenNumber = Integer.valueOf(request.getParameter(Attributes.NUMBER_OF_CHILDREN));
            if (childrenNumber < 0){
                childrenNumber = 0;
            }

            Employee employee = new Employee();
            employee.setNumber(Integer.valueOf(request.getParameter(Attributes.NUMBER).trim()));
            employee.setName(request.getParameter(Attributes.NAME));
            employee.setCnpp(cnpp);
            employee.setDepartment(request.getParameter(Attributes.DEPARTMENT));
            employee.setOccupation(request.getParameter(Attributes.OCCUPATION));
            employee.setDate(request.getParameter(Attributes.DATE));
            employee.setSingleParent(singleParent);
            employee.setNumberOfChildren(childrenNumber);

            EmployeeDao employeeDao = EmployeeDao.getInstance();
            employeeDao.update(employee);
        } catch (DaoException | NumberFormatException e) {
            logger.error(Messages.UPDATE_EMPLOYEE_FAIL, e);
            return PagePathes.PATH_TO_UPDATE_EMPL_PAGE;
        }
        request.getSession().setAttribute(Attributes.PAGE, PagePathes.PATH_TO_ALL_EMPLOYEE_PAGE);
        return PagePathes.PATH_TO_ALL_EMPLOYEE_PAGE;
    }
}
