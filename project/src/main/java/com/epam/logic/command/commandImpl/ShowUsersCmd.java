package com.epam.logic.command.commandImpl;

import com.epam.logic.command.Command;
import com.epam.constants.Attributes;
import com.epam.constants.Messages;
import com.epam.constants.PagePathes;
import com.epam.dao.daoimpl.AuthorizationDao;
import com.epam.exceptions.DaoException;
import com.epam.util.Messager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * This command performs users selection from DB
 * @author Эмма
 */
public class ShowUsersCmd implements Command{

    private static Logger logger = Logger.getLogger(ShowUsersCmd.class);

    @Override
    public String execute(HttpServletRequest request) {
        try {
            request.setAttribute(Attributes.ACCOUNTS_LIST, AuthorizationDao.getInstance().select());
        } catch (DaoException e) {
            e.printStackTrace();
            logger.error(Messages.SELECT_USERS_FAIL, e);
            Messager.sendMessage(request, Messages.UNCORRECT_INFORMATION);
        }
        return PagePathes.PATH_TO_ADMIN_PAGE;
    }
}
