package com.epam.logic.command.commandImpl;

import com.epam.beans.Employee;
import com.epam.logic.command.Command;
import com.epam.constants.Attributes;
import com.epam.constants.Messages;
import com.epam.constants.PagePathes;
import com.epam.dao.Dao;
import com.epam.dao.daoimpl.DismissedDao;
import com.epam.exceptions.DaoException;
import com.epam.util.Messager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * This command performs dismissed employees selection from DB
 * @author Эмма
 */
public class ShowDismissedCmd implements Command{
    private static Logger logger = Logger.getLogger(SelectPrepaymentsCmd.class);
    public static final int ENTRIES_NUMBER_ON_PAGE = 10;

    @Override
    public String execute(HttpServletRequest request) {
        int tpageTogo = 1;
        if (request.getParameter(Attributes.TABLE_PAGE_TO_GO) != null) {
            tpageTogo = Integer.parseInt(request.getParameter(Attributes.TABLE_PAGE_TO_GO));
        }

        try {
            Dao dismissedDao = DismissedDao.getInstance();
            List<Employee> list = dismissedDao.select((tpageTogo - 1) * ENTRIES_NUMBER_ON_PAGE,ENTRIES_NUMBER_ON_PAGE);
            request.setAttribute(Attributes.CURRENT_TABLE_PAGE, tpageTogo);
            int entriesCount = dismissedDao.getEntriesNumber();
            int tablePagesCount = (int) Math.ceil((double) entriesCount / ENTRIES_NUMBER_ON_PAGE);
            request.setAttribute(Attributes.TOTAL_TABLE_PAGES, tablePagesCount);
            request.setAttribute(Attributes.TOTAL_ENTRIES, entriesCount);
            request.setAttribute(Attributes.DISMISSED_LIST, list);
        } catch (DaoException e) {
            Messager.sendMessage(request, Messages.SERVER_ERROR);
            logger.error(e);
        }
        return PagePathes.PATH_TO_DISMISSED_EMPLOYEE_PAGE;
    }
}
