package com.epam.logic.command.commandImpl;

import com.epam.logic.command.Command;
import com.epam.constants.LangConstants;
import com.epam.constants.Messages;
import com.epam.util.ContentManager;
import com.epam.util.Messager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
/**
 *  This command changes language on current page and all app generally. Languages used - russian and english
 * @author Эмма
 */
public class ChangeLanguageCmd implements Command {
    private static Logger logger = Logger.getLogger(ChangeLanguageCmd.class);

    @Override
    public String execute(HttpServletRequest request) {
        String lang = request.getParameter(LangConstants.LANG_BUTTON);
        String path = request.getParameter(LangConstants.PATH);
        if (lang.equals(LangConstants.RU)) {
            request.getSession().setAttribute(LangConstants.BUNDLE, LangConstants.RU);
        } else if (lang.equals(LangConstants.EN)) {
            request.getSession().setAttribute(LangConstants.BUNDLE, LangConstants.EN);
        } else {
            logger.error(Messages.CNANGING_LANG_FAIL + path);
            Messager.sendMessage(request, Messages.LANG_ERROR);
        }
        ContentManager.addContent(path, request);
        return path;
    }
}
