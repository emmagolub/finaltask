package com.epam.logic.command;

import com.epam.logic.command.commandImpl.*;
import com.epam.logic.command.commandImpl.redirects.*;
import com.epam.constants.ButtonsCommands;

import java.util.HashMap;
import java.util.Map;

/**
 * Manages command extradition on demand
 * @author Эмма
 */
public class CommandManager {
    /**
     * Binds string parameter with command
     */
    private Map<String, Command> commands;
    private static CommandManager instance = new CommandManager();

    public static CommandManager getInstance(){
        return instance;
    }

    private CommandManager(){
        commands = new HashMap<>();
        commands.put(ButtonsCommands.LOGIN, new LoginCmd());
        commands.put(ButtonsCommands.ADD_EMPLOYEE_CMD, new AddEmployeeCmd());
        commands.put(ButtonsCommands.SHOW_EMPLOYEE, new AllEmployeeCmd());
        commands.put(ButtonsCommands.DISMISS_EMPLOYEE, new RemoveEmployeeCmd());
        commands.put(ButtonsCommands.EMPLOYEE_UPDATE_INFO, new ShowEmployeeToUpdateCmd());
        commands.put(ButtonsCommands.UPDATE_EMPLOYEE, new UpdateEmplCmd());
        commands.put(ButtonsCommands.REDIR_TO_UPDATE_EMPLOYEE, new RedirectToUpdateEmployee());
        commands.put(ButtonsCommands.SHOW_DISMISSED, new ShowDismissedCmd());
        commands.put(ButtonsCommands.REDIR_TO_ADD_SALARY, new RedirectToAddSalaryCmd());
        commands.put(ButtonsCommands.ADD_SALARY_INFO, new ShowEmployeeToAddSalaryCmd());
        commands.put(ButtonsCommands.ADD_SALARY, new AddSalaryCmd());
        commands.put(ButtonsCommands.ADD_VACATION, new AddVacationCmd());
        commands.put(ButtonsCommands.ADD_PREPAYMENT, new AddPrepaymentCmd());
        commands.put(ButtonsCommands.SHOW_SALARIES, new SelectSalariesCmd());
        commands.put(ButtonsCommands.SHOW_PREPAYMENTS, new SelectPrepaymentsCmd());
        commands.put(ButtonsCommands.SHOW_VACATIONS, new SelectVacationsCmd());
        commands.put(ButtonsCommands.LOGOUT, new LogOutCmd());
        commands.put(ButtonsCommands.LOCK_USER, new LockUserCmd());
        commands.put(ButtonsCommands.UNLOCK_USER, new UnlockUserCmd());
        commands.put(ButtonsCommands.ADD_USER, new AddUserCmd());
        commands.put(ButtonsCommands.REDIR_TO_ADD_EMPLOYEE, new RedirectToAddEmployeeCmd());
        commands.put(ButtonsCommands.REDIR_TO_ADD_VACATION, new RedirectToAddVacationCmd());
        commands.put(ButtonsCommands.REDIR_TO_REMOVE_EMPLOYEE, new RedirectToRemoveEmployeeCmd());
        commands.put(ButtonsCommands.REDIR_TO_ADD_PREPAYMENT, new RedirectToAddPrepayment());
        commands.put(ButtonsCommands.CHANGE_LANG, new ChangeLanguageCmd());
        commands.put(ButtonsCommands.REDIR_TO_SALARIES_SEARCH, new RedirectToSalariesSearch());
        commands.put(ButtonsCommands.SALARY_SEARCH, new SalariesSearchCmd());
        commands.put(ButtonsCommands.REDIR_TO_PREPAYMENTS_SEARCH, new RedirectToPrepaymentsSearchCmd());
        commands.put(ButtonsCommands.REDIR_TO_VACATIONS_SEARCH, new RedirectToVacationsSearchCmd());
        commands.put(ButtonsCommands.PREPAYMENTS_SEARCH, new PrepaymentsSearchCmd());
        commands.put(ButtonsCommands.VACATIONS_SEARCH, new VacationsSearchCmd());
    }

    /**
     * Get command from manager by its string name
     * @param commandString command's name
     * @return Command needed
     */
    public Command getCommand(String commandString){
        Command result = commands.get(commandString);
        return result;
    }
}
