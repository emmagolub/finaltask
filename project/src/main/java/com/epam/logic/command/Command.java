package com.epam.logic.command;

import javax.servlet.http.HttpServletRequest;

/**
 * This class defines interface methods which Command pattern classes should implement
 * @author Эмма
 */
public interface Command {
    /**
     * Perform some logics with request
     * @param request servlet request where/from put/get data
     * @return Page path string, which define where will bee request and response forwarded or redirected
     */
    String execute (HttpServletRequest request);
}
