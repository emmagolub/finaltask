package com.epam.logic.command.commandImpl;

import com.epam.logic.command.Command;
import com.epam.constants.AccessConstants;
import com.epam.constants.PagePathes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Log current user out
 * @author Эмма
 */
public class LogOutCmd implements Command {
    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if(null != session) {
            session.removeAttribute(AccessConstants.ROLE);
        }
        return PagePathes.PATH_TO_LOGIN_PAGE;
    }
}
