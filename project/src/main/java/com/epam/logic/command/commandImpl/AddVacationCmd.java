package com.epam.logic.command.commandImpl;

import com.epam.beans.Employee;
import com.epam.beans.Vacation;
import com.epam.logic.command.Command;
import com.epam.constants.Attributes;
import com.epam.constants.Messages;
import com.epam.constants.PagePathes;
import com.epam.dao.daoimpl.VacationDao;
import com.epam.exceptions.DaoException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;

/**
 * This command performs new vacation addition to DB
 * @author Эмма
 */
public class AddVacationCmd implements Command {
    private static Logger logger = Logger.getLogger(AddVacationCmd.class);

    @Override
    public String execute(HttpServletRequest request) {
        try {
            Employee employee = new Employee();
            employee.setNumber(Integer.parseInt(request.getParameter(Attributes.NUMBER)));
            Vacation vacation = new Vacation();
            vacation.setEmployee(employee);
            vacation.setSum(Integer.parseInt(request.getParameter(Attributes.SUM)));
            vacation.setDate(Date.valueOf(request.getParameter(Attributes.DATE)));
            vacation.setPeriod(
                    request.getParameter(Attributes.START_DATE) + " - " + request.getParameter(Attributes.END_DATE));
            VacationDao.getInstance().insert(vacation);
            logger.info(Messages.VACATION_ADDED);
        }
        catch(DaoException | NumberFormatException e) {
            logger.error(e.getMessage(), e);
            return PagePathes.PATH_TO_ADD_VACATION_PAGE;
        }
        request.getSession().setAttribute(Attributes.PAGE, PagePathes.PATH_TO_VACATIONS_HISTORY);
        return PagePathes.PATH_TO_VACATIONS_HISTORY;
    }
}
