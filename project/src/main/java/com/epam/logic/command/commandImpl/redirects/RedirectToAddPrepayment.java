package com.epam.logic.command.commandImpl.redirects;

import com.epam.logic.command.Command;
import com.epam.constants.AccessConstants;
import com.epam.constants.PagePathes;

import javax.servlet.http.HttpServletRequest;

/**
 * This command performs redirection to prepayment adding page
 * @author Эмма
 */
public class RedirectToAddPrepayment implements Command {
    @Override
    public String execute(HttpServletRequest request) {
        if(request.getSession().getAttribute(AccessConstants.ROLE).equals(AccessConstants.USER_ROLE)) {
            return PagePathes.PATH_TO_ADD_PREPAYMENT_PAGE;
        }else return PagePathes.PATH_TO_ERROR_PAGE;
    }
}
