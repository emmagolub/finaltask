package com.epam.logic.command.commandImpl;

import com.epam.beans.DismissedEmployee;
import com.epam.constants.Attributes;
import com.epam.constants.Messages;
import com.epam.constants.PagePathes;
import com.epam.dao.daoimpl.DismissedDao;
import com.epam.exceptions.DaoException;
import com.epam.logic.command.Command;
import com.epam.util.Messager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Deletes employee from DB by his personal number number
 * @author Эмма
 */
public class RemoveEmployeeCmd implements Command {
    private static Logger logger = Logger.getLogger(RemoveEmployeeCmd.class);

    @Override
    public String execute(HttpServletRequest request) {
       try {
           DismissedDao dismissedDao = DismissedDao.getInstance();
           DismissedEmployee dismissedEmployee = new DismissedEmployee();
           dismissedEmployee.setNumber(Integer.parseInt(request.getParameter(Attributes.PERSONAL_NUMBER)));
           dismissedEmployee.setFinishDate(request.getParameter(Attributes.DISMISSING_DATE));
           dismissedDao.update(dismissedEmployee);
        }
        catch(DaoException | NumberFormatException e) {
            logger.error(Messages.EMPLOYEE_REMOVING_PROBLEM, e);
            Messager.sendMessage(request, Messages.WRONG_EMPLOYEE_NUMBER);
            return PagePathes.PATH_TO_REMOVE_EMPLOYEE_PAGE;
        }
        return PagePathes.PATH_TO_DISMISSED_EMPLOYEE_PAGE;
    }
}
