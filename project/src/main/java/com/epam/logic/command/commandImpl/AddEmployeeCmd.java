package com.epam.logic.command.commandImpl;

import com.epam.beans.Employee;
import com.epam.logic.command.Command;
import com.epam.constants.Attributes;
import com.epam.constants.Messages;
import com.epam.constants.PagePathes;
import com.epam.dao.daoimpl.EmployeeDao;
import com.epam.exceptions.DaoException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * This command performs new employee addition to db
 * @author Эмма
 */
public class AddEmployeeCmd implements Command {
    private static Logger logger = Logger.getLogger(AddEmployeeCmd.class);
    @Override
    public String execute(HttpServletRequest request) {
        Boolean cnpp = false, singleParent = false;
        if (request.getParameter(Attributes.CNPP) != null) {
            cnpp = true;
        }
        if (request.getParameter(Attributes.SINGLE_PARENT) != null){
            singleParent = true;
        }
        int childrenNumber = Integer.valueOf(request.getParameter(Attributes.NUMBER_OF_CHILDREN));
        if (childrenNumber < 0){
            childrenNumber = 0;
        }

        try {
            Employee employee = new Employee(
                    cnpp,
                    singleParent,
                    childrenNumber,
                    request.getParameter(Attributes.DEPARTMENT),
                    request.getParameter(Attributes.OCCUPATION),
                    request.getParameter(Attributes.DATE),
                    request.getParameter(Attributes.NAME)
            );
            EmployeeDao employeeDao = EmployeeDao.getInstance();
            employeeDao.insert(employee);
            logger.info(Messages.EMPLOYEE_ADDED);
        } catch (DaoException | NumberFormatException e) {
            logger.error(e.getMessage(), e);
            return PagePathes.PATH_TO_ADD_EMPLOYEE_PAGE;
        }
        request.getSession().setAttribute(Attributes.PAGE, PagePathes.PATH_TO_ALL_EMPLOYEE_PAGE);
        return PagePathes.PATH_TO_ALL_EMPLOYEE_PAGE;
    }
}
