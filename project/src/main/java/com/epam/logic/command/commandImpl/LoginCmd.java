package com.epam.logic.command.commandImpl;


import com.epam.beans.Account;
import com.epam.logic.command.Command;
import com.epam.constants.AccessConstants;
import com.epam.constants.Attributes;
import com.epam.constants.Messages;
import com.epam.constants.PagePathes;
import com.epam.dao.daoimpl.AuthorizationDao;
import com.epam.exceptions.DaoException;
import com.epam.passwordmanager.PasswordManager;
import com.epam.util.Messager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * This command identifies is login/password correct and who is the user.
 * Uses <code>PasswordManager</code> to compare passwords
 * @author Эмма
 */
public class LoginCmd implements Command {

    private static int UNLOCKED_STATUS_ID = 1;
    private static int ADMIN_ROLE_ID = 1;

    @Override
    public String execute(HttpServletRequest request) {
        AuthorizationDao authorizationDao = AuthorizationDao.getInstance();
        String login = request.getParameter(Attributes.LOGIN);
        String password = request.getParameter(Attributes.PASSWORD);
        PasswordManager passwordManager = PasswordManager.getInstance();
        HttpSession session = request.getSession(true);
        try {
            Account account = authorizationDao.select(login);
            if (account == null) {
                session.setAttribute(AccessConstants.ROLE, AccessConstants.GUEST_ROLE);
                return PagePathes.PATH_TO_LOGIN_PAGE;
            }
            if (passwordManager.comparePasswords(password, account.getPassword())) {
                if (account.getStatusId() == UNLOCKED_STATUS_ID) {
                    if (account.getRoleId() == ADMIN_ROLE_ID) {
                        session.setAttribute(AccessConstants.ROLE, AccessConstants.ADMIN_ROLE);
                        return PagePathes.PATH_TO_ADMIN_PAGE;
                    } else {
                        session.setAttribute(AccessConstants.ROLE, AccessConstants.USER_ROLE);
                        session.setAttribute(Attributes.PAGE, PagePathes.PATH_TO_SALARIES_HISTORY);
                        return new SelectSalariesCmd().execute(request);
                    }
                } else {
                    session.setAttribute(AccessConstants.ROLE, AccessConstants.GUEST_ROLE);
                    Messager.sendMessage(request, Messages.AUTHORIZATION_ERROR);
                    return PagePathes.PATH_TO_LOGIN_PAGE;
                }
            }
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return PagePathes.PATH_TO_LOGIN_PAGE;
    }
}
