package com.epam.logic.command.commandImpl;

import com.epam.beans.Account;
import com.epam.logic.command.Command;
import com.epam.constants.Attributes;
import com.epam.constants.Messages;
import com.epam.constants.PagePathes;
import com.epam.dao.daoimpl.AuthorizationDao;
import com.epam.exceptions.DaoException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 *  This command locks user: updates his status in DB
 * @author Эмма
 */
public class LockUserCmd implements Command {
    private static Logger logger = Logger.getLogger(LockUserCmd.class);

    @Override
    public String execute(HttpServletRequest request) {
        try {
            Account account = new Account();
            account.setLogin(request.getParameter(Attributes.LOGIN_TO_LOCK));
            account.setStatusId(2);
            AuthorizationDao.getInstance().update(account);
        } catch (DaoException | NumberFormatException e) {
            logger.error(Messages.UPDATE_USER_FAIL, e);
            return PagePathes.PATH_TO_USER_ERROR_PAGE;
        }
        return PagePathes.PATH_TO_ADMIN_PAGE;
    }
}
