package com.epam.logic.command.commandImpl;

import com.epam.beans.Employee;
import com.epam.logic.command.Command;
import com.epam.constants.Attributes;
import com.epam.constants.Messages;
import com.epam.constants.PagePathes;
import com.epam.dao.daoimpl.EmployeeDao;
import com.epam.exceptions.DaoException;
import com.epam.util.Messager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * This command performs employees selection from DB to preview it to salary addition
 * @author Эмма
 */
public class ShowEmployeeToAddSalaryCmd  implements Command{
    private static Logger logger = Logger.getLogger(ShowEmployeeToAddSalaryCmd.class);

    @Override
    public String execute(HttpServletRequest request) {
        int emplPersNumb = 0;
        try {
            emplPersNumb = Integer.valueOf(request.getParameter(Attributes.ID).trim());
        } catch (NumberFormatException e){
            logger.error(Messages.PREVIEW_EMPLOYEES_FAIL,e);
            Messager.sendMessage(request, Messages.WRONG_EMPLOYEE_NUMBER);
            return PagePathes.PATH_TO_ADD_SALARY_PAGE;
        }

        EmployeeDao employeeDao = EmployeeDao.getInstance();
        try {
            Employee employee = employeeDao.select(emplPersNumb);
            if (employee != null){
                request.setAttribute(Attributes.NUMBER, emplPersNumb);
                request.setAttribute(Attributes.NAME, employee.getName());
                request.setAttribute(Attributes.DEPARTMENT, employee.getDepartment());
                request.setAttribute(Attributes.OCCUPATION, employee.getOccupation());
            }else {
                logger.warn(Messages.PREVIEW_EMPLOYEES_FAIL + Messages.BECAUSE_IT_NULL);
                Messager.sendMessage(request, Messages.WRONG_EMPLOYEE_NUMBER);
            }
        } catch (DaoException e) {
            logger.error(Messages.PREVIEW_EMPLOYEES_FAIL, e);
            Messager.sendMessage(request, Messages.WRONG_EMPLOYEE_NUMBER);
        }

        return PagePathes.PATH_TO_ADD_SALARY_PAGE;
    }
}
