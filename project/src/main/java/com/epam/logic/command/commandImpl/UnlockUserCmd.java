package com.epam.logic.command.commandImpl;

import com.epam.beans.Account;
import com.epam.logic.command.Command;
import com.epam.constants.Attributes;
import com.epam.constants.Messages;
import com.epam.constants.PagePathes;
import com.epam.dao.daoimpl.AuthorizationDao;
import com.epam.exceptions.DaoException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
/**
 *  This command unlocks user: updates his status in DB
 * @author Эмма
 */
public class UnlockUserCmd implements Command {
    private static Logger logger = Logger.getLogger(UnlockUserCmd.class);

    @Override
    public String execute(HttpServletRequest request) {

        try {
            Account account = new Account();
            account.setLogin(request.getParameter(Attributes.LOGIN_TO_UNLOCK));
            account.setStatusId(1);
            AuthorizationDao.getInstance().update(account);
        } catch (DaoException | NumberFormatException e) {
            logger.error(Messages.UPDATE_USER_FAIL, e);
            e.printStackTrace();
            return PagePathes.PATH_TO_USER_ERROR_PAGE;
        }
        return PagePathes.PATH_TO_ADMIN_PAGE;
    }
}
