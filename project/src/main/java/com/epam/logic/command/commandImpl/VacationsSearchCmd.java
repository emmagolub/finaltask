package com.epam.logic.command.commandImpl;

import com.epam.beans.Employee;
import com.epam.beans.Vacation;
import com.epam.logic.command.Command;
import com.epam.constants.Attributes;
import com.epam.constants.Messages;
import com.epam.constants.PagePathes;
import com.epam.dao.Dao;
import com.epam.dao.daoimpl.VacationDao;
import com.epam.exceptions.DaoException;
import com.epam.util.Messager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * Searches vacations in DB by employee number
 * @author Эмма
 */
public class VacationsSearchCmd implements Command {
    private static Logger logger = Logger.getLogger(VacationsSearchCmd.class);
    public static final int ENTRIES_NUMBER_ON_PAGE = 10;

    @Override
    public String execute(HttpServletRequest request) {
        int tpageTogo = 1;
        int employeeNumber = 1;


        try {
            if (request.getParameter(Attributes.TABLE_PAGE_TO_GO) != null) {
                tpageTogo = Integer.parseInt(request.getParameter(Attributes.TABLE_PAGE_TO_GO));
                employeeNumber = Integer.parseInt(request.getParameter(Attributes.EMPLOYEE_NUMBER).trim());
            }
            if(!request.getParameter(Attributes.NUMBER).trim().equals("")){
                employeeNumber = Integer.parseInt(request.getParameter(Attributes.NUMBER).trim());
            }
            Employee employee = new Employee();
            employee.setNumber(employeeNumber);
            Vacation vacation = new Vacation();
            vacation.setEmployee(employee);
            Dao vacationDao = VacationDao.getInstance();
            List<Vacation> list =  ((VacationDao)vacationDao).select(vacation, (tpageTogo - 1) * ENTRIES_NUMBER_ON_PAGE,ENTRIES_NUMBER_ON_PAGE);
            request.setAttribute(Attributes.CURRENT_TABLE_PAGE, tpageTogo);
            int entriesCount = ((VacationDao)vacationDao).getEntriesNumber(employeeNumber);

            int tablePagesCount = (int) Math.ceil((double) entriesCount / ENTRIES_NUMBER_ON_PAGE);
            request.setAttribute(Attributes.TOTAL_TABLE_PAGES, tablePagesCount);
            request.setAttribute(Attributes.TOTAL_ENTRIES, entriesCount);
            request.setAttribute(Attributes.VACATIONS_LIST, list);
            request.setAttribute(Attributes.EMPLOYEE_NUMBER, employeeNumber);
        } catch (DaoException | NumberFormatException e) {
            Messager.sendMessage(request, Messages.WRONG_EMPLOYEE_NUMBER);
            logger.error(e);
        }

        return PagePathes.PATH_TO_VACATIONS_SEARCH;
    }
}
