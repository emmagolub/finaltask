package com.epam.connectionspool;

import com.epam.constants.DBConstants;
import com.epam.constants.ExceptionConstants;
import com.epam.constants.ResourcesPathes;
import com.epam.exceptions.ConnectionLevelException;
import com.epam.util.PropertiesManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class creates certain amount of connections and manage them
 * @author Emma
 */
public class ConnectionPool {
    private static ConnectionPool instance;
    private BlockingQueue<Connection> freeConnections;
    private BlockingQueue<Connection> busyConnections;
    PropertiesManager dbResMng;
    private static ReentrantLock lock = new ReentrantLock();

    public static ConnectionPool getInstance() throws ConnectionLevelException {
        lock.lock();
        if(instance == null) {
            instance = new ConnectionPool();
        }
        lock.unlock();
        return instance;
    }

    private ConnectionPool() throws ConnectionLevelException {
        dbResMng = new PropertiesManager(ResourcesPathes.DB_RESOURCE_LOCATION);
        int size = Integer.parseInt(dbResMng.getValue(DBConstants.DB_POOLSIZE));
        freeConnections = new ArrayBlockingQueue<>(size);
        busyConnections = new ArrayBlockingQueue<>(size);

        try {
            Class.forName(dbResMng.getValue(DBConstants.DB_DRIVER));

            for(int i = 0; i < size; i++) {
                Connection connection = DriverManager.getConnection(dbResMng.getValue(DBConstants.DB_URL),
                                                                    dbResMng.getValue(DBConstants.DB_USER),
                                                                    dbResMng.getValue(DBConstants.DB_PASSWORD)
                                                                   );
                freeConnections.add(connection);
            }
        }
        catch(ClassNotFoundException e) {
            throw new ConnectionLevelException(ExceptionConstants.DB_DRIVER_NOT_FOUND, e);
        }
        catch(SQLException e) {
            throw new ConnectionLevelException(ExceptionConstants.CP_INIT_FAIL, e);
        }
    }

    /**
     * Get free connection from pool
     * @return Connection
     * @throws ConnectionLevelException
     */
    public Connection getFreeConnection() throws ConnectionLevelException {
        Connection connection = null;
        try {
            connection = freeConnections.take();
            busyConnections.add(connection);
        }
        catch(InterruptedException e) {
            throw new ConnectionLevelException(ExceptionConstants.TAKING_INTERRUPTED + e.getMessage(), e);
        }
        return connection;
    }
    /**
     * Puts connection back to pool of free connections
     * @param connection Connection to be put back to pool
     * @throws ConnectionLevelException
     */
    public void freeConnection(Connection connection) throws ConnectionLevelException {
        if(busyConnections.contains(connection)) {
            try {
                freeConnections.put(connection);
                busyConnections.remove(connection);
            }
            catch(InterruptedException e) {
                throw new ConnectionLevelException(ExceptionConstants.CANT_FREE_CONNECTION + e.getMessage(), e);
            }
        }
        else {
            throw new ConnectionLevelException(ExceptionConstants.NONPOOLED_CONNECTION);
        }
    }

    /**
     * Closes all opened connections
     * @throws ConnectionLevelException
     */
    public void closeAll() throws ConnectionLevelException {
        try {
            for(Connection freeConnection : freeConnections) {
                freeConnection.close();
            }
            for(Connection busyConnection : busyConnections) {
                busyConnection.close();
            }
        }
        catch(SQLException e) {
            throw new ConnectionLevelException(ExceptionConstants.CP_CLOSE_FAIL, e);
        }
    }
}
