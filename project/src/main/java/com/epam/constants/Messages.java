package com.epam.constants;

/**
 * @author Эмма
 */
public class Messages {
    public static final String EMPLOYEE_ADDED = "New employee was added to database";
    public static final String PREPAYMENT_ADDED = "New prepayment was added to database";
    public static final String SALARY_ADDED = "New salary was added to database";
    public static final String VACATION_ADDED = "New vacation was added to database";
    public static final String SELECT_EMPLOYEE_FAIL = "Problem with employee printing";
    public static final String EMPLOYEE_SEARCH_FAIL = "Problem with employee searching";
    public static final String EMPLOYEE_REMOVING_PROBLEM = "Problem with employee removing";
    public static final String BECAUSE_IT_NULL = " because it null";
    public static final String SELECT_PAYMENTS_FAIL = "Problem with selecting payments";
    public static final String RESULT_SALARY = "Result Salary = ";
    public static final String SALARY_TAX = "Salary Tax = ";
    public static final String PENSION_FUND = "Pension Fund = ";
    public static final String CLEAN_SALARY = "Clean Salary = ";
    public static final String SELECT_PREPAYMENTS_FAIL = "Problem with selecting prepayments";
    public static final String SELECT_SALRIES_FAIL = "Problem with selecting salaries";
    public static final String SELECT_VACATIONS_FAIL = "Problem with selecting vacations";
    public static final String SELECT_DISMISSED_FAIL = "Problem with selecting dismissed";
    public static final String SELECT_USERS_FAIL = "Problem with selecting users";
    public static final String PREVIEW_EMPLOYEES_FAIL = "Problem with previewing eployees";
    public static final String UPDATE_EMPLOYEE_FAIL = "Problem with updating employee";
    public static final String UPDATE_USER_FAIL = "Problem with updating user. Maybe it is a wrong login";
    public static final String ADD_USER_FAIL = "Problem with adding user. Maybe it is a wrong login";
    public static final String CNANGING_LANG_FAIL = "Cannot change language on the page ";

    public static final String WRONG_EMPLOYEE_NUMBER = "wrong.employee.number";
    public static final String UNCORRECT_INFORMATION = "uncorrect.information";
    public static final String SERVER_ERROR = "server.error";
    public static final String LANG_ERROR = "changing.lang.error";
    public static final String AUTHORIZATION_ERROR = "authorization.error";
}
