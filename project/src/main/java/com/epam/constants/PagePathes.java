package com.epam.constants;

/**
 * @author Эмма
 */
public class PagePathes {
    public static final String PATH_TO_ADMIN_PAGE = "html/admin.jsp";
    public static final String PATH_TO_LOGIN_PAGE = "login.jsp";
    public static final String PATH_TO_ADD_SALARY_PAGE = "html/addSalary.jsp";
    public static final String PATH_TO_ALL_EMPLOYEE_PAGE = "html/allEmployee.jsp";
    public static final String PATH_TO_UPDATE_EMPL_PAGE = "html/updateEmployee.jsp";
    public static final String PATH_TO_DISMISSED_EMPLOYEE_PAGE = "html/dismissed.jsp";
    public static final String PATH_TO_ADD_VACATION_PAGE = "html/addVocation.jsp";
    public static final String PATH_TO_ADD_PREPAYMENT_PAGE = "html/addPrepayment.jsp";
    public static final String PATH_TO_REMOVE_EMPLOYEE_PAGE = "html/dismissEmployee.jsp";
    public static final String PATH_TO_ADD_EMPLOYEE_PAGE = "html/addEmployee.jsp";
    public static final String PATH_TO_SALARIES_HISTORY = "html/salariesHistory.jsp";
    public static final String PATH_TO_PREPAYMENTS_HISTORY = "html/prepaymentsHistory.jsp";
    public static final String PATH_TO_VACATIONS_HISTORY = "html/vacationsHistory.jsp";
    public static final String PATH_TO_ERROR_PAGE = "404.jsp";
    public static final String PATH_TO_USER_ERROR_PAGE = "400.jsp";
    public static final String PATH_TO_SALARIES_SEARCH = "html/salariesSearch.jsp";
    public static final String PATH_TO_VACATIONS_SEARCH = "html/vacationsSearch.jsp";
    public static final String PATH_TO_PREPAYMENTS_SEARCH = "html/prepaymentsSearch.jsp";
}
