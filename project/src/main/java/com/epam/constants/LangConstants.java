package com.epam.constants;

/**
 * @author Эмма
 */
public class LangConstants {
    public static final String BUNDLE = "bundle";
    public static final String RU = "ru_Ru";
    public static final String EN = "en_En";
    public static final String LANG_BUTTON = "langBtn";
    public static final String PATH = "path";
}
