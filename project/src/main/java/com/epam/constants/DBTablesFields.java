package com.epam.constants;

/**
 * @author Эмма
 */
public class DBTablesFields {
    public static final String ID = "ID";
    public static final String NUMBER = "number";
    public static final String NAME = "name";
    public static final String DEPARTMENT = "department";
    public static final String OCCUPATION = "occupation";
    public static final String DATE = "date";
    public static final String NUMBER_OF_CHILDREN = "number_of_children";
    public static final String SINGLE_PARENT = "single_parent";
    public static final String AFFECTED_BY_CNPP = "affected_by_CNPP";
    public static final String LOGIN = "login";
    public static final String PASSWORD= "password";
    public static final String SUM = "sum";
    public static final String COMMENT = "comment";
    public static final String START_DATE = "start_work_day";
    public static final String FINISH_DATE = "end_work_day";
    public static final String ROLE = "role_id";
    public static final String ROLE_NAME = "role_name";
    public static final String STATUS = "status_id";
    public static final String STATUS_NAME = "status_name";
    public static final String EMPLOYEE_NAME = "name";
    public static final String SALARY_ID = "id";
    public static final String SALARY_SUM = "sum";
    public static final String SALARY_DATE = "date";


}
