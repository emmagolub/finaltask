package com.epam.constants;

/**
 * @author Эмма
 */

public class ButtonsCommands{
    public static final String COMMAND = "cmd";
    //login page
    public static final String LOGIN = "login";
    public static final String LOGOUT = "logOut";


    //main page
    public static final String SHOW_EMPLOYEE = "showEmployee";
    public static final String REDIR_TO_HISTORY = "redirectToHistory";
    public static final String REDIR_TO_EMPLOYEE_SEARCH = "redirectToEmployeeSearch";
    public static final String SHOW_DISMISSED = "showDismissed";
    public static final String REDIR_TO_UPDATE_EMPLOYEE = "redirectToUpdateEmployee";
    public static final String REDIR_TO_ADD_SALARY = "redirectToAddSalary";
    public static final String REDIR_TO_ADD_PREPAYMENT = "redirectToAddPrepayment";
    public static final String REDIR_TO_REMOVE_EMPLOYEE = "redirectToRemoveEmployee";
    public static final String REDIR_TO_ADD_VACATION = "redirectToAddVacation";
    public static final String REDIR_TO_ADD_EMPLOYEE = "redirectToAddEmployee";
    public static final String REDIR_TO_SALARIES_SEARCH = "redirectToSalariesSearch";
    public static final String REDIR_TO_PREPAYMENTS_SEARCH = "redirectToPrepaymentsSearch";
    public static final String REDIR_TO_VACATIONS_SEARCH = "redirectToVacationsSearch";



    //addEmployee page
    public static final String ADD_EMPLOYEE_CMD = "addEmployeeCmd";

    //updateEmployee page
    public static final String EMPLOYEE_UPDATE_INFO = "employeeUpdateInfo";
    public static final String UPDATE_EMPLOYEE = "updateEmployee";
    public static final String ADD_SALARY_INFO = "addSalaryInfo";

    //dismissEmployee page
    public static final String DISMISS_EMPLOYEE = "dismissEmployee";

    public static final String ADD_SALARY = "addSalary";
    public static final String ADD_VACATION = "addVacation";
    public static final String ADD_PREPAYMENT = "addPrepayment";

    public static final String SELECT_SALARY = "selectSalary";
    public static final String SELECT_PREPAYMENTS = "selectPrepayments";
    public static final String SELECT_VACATIONS = "selectVacations";
    public static final String SELECT_ALL_PAYMENTS = "selectAllPayments";

    public static final String EMPLOYEE_SEARCH = "employeeSearch";
    public static final String ADD_USER = "addUser";
    public static final String LOCK_USER = "lockUser";
    public static final String UNLOCK_USER = "unlockUser";

    public static final String CHANGE_LANG = "changeLang";
    public static final String REDIR_TO_MAIN_PAGE = "redirectToMainPage";

    public static final String SHOW_SALARIES = "showSalaries";
    public static final String SHOW_PREPAYMENTS = "showPrepayments";
    public static final String SHOW_VACATIONS = "showVacations";

    public static final String SALARY_SEARCH = "salariesSearch";
    public static final String PREPAYMENTS_SEARCH = "prepaymentsSearch";
    public static final String VACATIONS_SEARCH = "vacationsSearch";
}
