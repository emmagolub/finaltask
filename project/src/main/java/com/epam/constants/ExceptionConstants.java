package com.epam.constants;

/**
 * @author Эмма
 */
public class ExceptionConstants{
    public static final String SQL_FAIL = "Cannot execute sql ";
    public static final String CANT_FREE_CONNECTION = "Cannot free connection ";
    public static final String CONNECTION_FAIL = "Connection fail ";
    public static final String DB_DRIVER_NOT_FOUND = "db driver not found ";
    public static final String CP_INIT_FAIL = "Connection pool initialization failed ";
    public static final String CP_CLOSE_FAIL = "Connection pool close fail ";
    public static final String NONPOOLED_CONNECTION = "Tried to close nonpooled connection ";
    public static final String TAKING_INTERRUPTED = "Interrupted during taking connection ";
    public static final String VACATION = "Cannot add vacation mote than 3 times a year";
}
