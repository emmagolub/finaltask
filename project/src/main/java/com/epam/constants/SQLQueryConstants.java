package com.epam.constants;

/**
 *@author Эмма
 */
public class SQLQueryConstants {
    public static final String INSERT_AUTHORIZATION = "INSERT INTO user" +
            " (login, role_id, status_id, password) VALUES" +
            " (?,?,?,?)";
    public static final String SELECT_BY_LOGIN_FROM_AUTHORIZATION = "Select* from user where login = ?";
    public static final String SELECT_ALL_FROM_AUTHORIZATION = "Select user.login, user_role.role_name, user_status.status_name  from user" +
            " inner join user_role on user.role_id = user_role.id" +
            " inner join user_status on user.status_id = user_status.id";
    public static final String UPDATE_AUTHORIZATION = "UPDATE user SET status_id = ?"
            + " WHERE login = ?";



    public static final String INSERT_INTO_PRIVATE_EMPLOYEE_INFO = "insert into private_employee_info (name, single_parent, affected_by_CNPP, number_of_children)" +
            " values (?, ?, ?, ?)";
    public static final String INSERT_INTO_EMPLOYEE_INFO = "insert into employee_info (department, occupation, employee_status_id, start_work_day)" +
            " values (?, ?, 1, ?)";

    public static final String SELECT_EMPLOYEE_NUMBER = "select id from employee order by id desc limit 1";

    public static final String INSERT_INTO_EMPLOYEE = "insert into employee (employee_info_id, private_employee_info_id) values (?, ?)";

    public static final String SELECT_ALL_FROM_EMPLOYEE = "select* from employee" +
            " inner join employee_info on employee.employee_info_id = employee_info.id" +
            " inner join private_employee_info on employee.private_employee_info_id = private_employee_info.id" +
            " where employee_status_id = 1";


    public static final String SELECT_INTERVAL_FROM_EMPLOYEE = "select* from employee" +
            " inner join employee_info on employee.employee_info_id = employee_info.id" +
            " inner join private_employee_info on employee.private_employee_info_id = private_employee_info.id " +
            " where employee_status_id = 1 limit ?, ?";
    public static final String SELECT_FROM_EMPLOYEE_BY_NUMBER = "select* from employee" +
            " inner join employee_info on employee.employee_info_id = employee_info.id" +
            " inner join private_employee_info on employee.private_employee_info_id = private_employee_info.id where employee.id = ?";

    public static final String UPDATE_EMPLOYEE_STATUS = "update employee_info set employee_status_id = 2, end_work_day = ? where id = ?";

    public static final String UPDATE_EMPLOYEE_INFO = "update employee_info set start_work_day = ?, department = ?, occupation = ? where id = ?";
    public static final String UPDATE_PRIVATE_EMPLOYEE_INFO = "update private_employee_info set name = ?, single_parent = ?, affected_by_CNPP = ?, number_of_children = ? where id = ?";


    public static final String SELECT_ALL_FROM_DISMISSED = "select* from employee" +
            " inner join employee_info on employee.employee_info_id = employee_info.id" +
            " inner join private_employee_info on employee.private_employee_info_id = private_employee_info.id" +
            " where employee_status_id = 2";
    public static final String SELECT_INTERVAL_FROM_DISMISSED = "select* from employee" +
            " inner join employee_info on employee.employee_info_id = employee_info.id" +
            " inner join private_employee_info on employee.private_employee_info_id = private_employee_info.id" +
            " where employee_status_id = 2 limit ?, ?";




    public static final String INSERT_INTO_PREPAYMENT = "INSERT INTO payment"
            + "(payment_kind_id, employee_id, date, sum) VALUES"
            + "(3,?,?,?)";
    public static final String SELECT_ALL_FROM_PREPAYMENT =" select payment.id, payment.date, payment.sum, private_employee_info.name" +
            " from  payment,  employee,  private_employee_info" +
            " where  payment.employee_id = employee.id and employee.private_employee_info_id = private_employee_info.id and payment_kind_id = 3" +
            " order by payment.id desc";
    public static final String SELECT_INTERVAL_FROM_PREPAYMENT = "select payment.id, payment.date, payment.sum, private_employee_info.name" +
            " from  payment,  employee,  private_employee_info" +
            " where  payment.employee_id = employee.id and employee.private_employee_info_id = private_employee_info.id and payment_kind_id = 3" +
            " order by payment.id desc limit ?,?";

    public static final String SELECT_BY_EMPL_NUMBER_FROM_PREPAYMENT = "Select* from Payment WHERE employee_id = ? and payment_kind_id = 3 limit ?,?";
    public static final String SELECT_ALL_FROM_PREPAYMENT_BY_EMPL_NUMBER = "Select* from Payment WHERE employee_id = ? and payment_kind_id = 3";


    public static final String INSERT_INTO_SALARY = "INSERT INTO payment(payment_kind_id, employee_id, date, sum) VALUES(1, ?, ?, ?)";
    public static final String SELECT_FROM_SALARY_BY_EMPL_NUMBER = "Select* from Payment WHERE employee_id = ? and payment_kind_id = 1 limit ?,?";
    public static final String SELECT_ALL_FROM_SALARY_BY_EMPL_NUMBER = "Select* from Payment WHERE employee_id = ? and payment_kind_id = 1";
    public static final String SELECT_ALL_FROM_SALARY = " select payment.id, payment.date, payment.sum, private_employee_info.name" +
            " from  payment,  employee,  private_employee_info" +
            " where  payment.employee_id = employee.id and employee.private_employee_info_id = private_employee_info.id and payment_kind_id = 1" +
            " order by payment.id desc";
    public static final String SELECT_INTERVAL_FROM_SALARY =  "select payment.id, payment.date, payment.sum, private_employee_info.name" +
            " from  payment,  employee,  private_employee_info" +
            " where  payment.employee_id = employee.id and employee.private_employee_info_id = private_employee_info.id and payment_kind_id = 1" +
            " order by payment.id desc limit ?,?";


    public static final String INSERT_INTO_VACATION = "INSERT INTO payment(payment_kind_id, employee_id, date, sum, comment) VALUES(2, ?, ?, ?, ?)";
    public static final String SELECT_FROM_VACATION_BY_EMPL_NUMBER = "Select* from Payment WHERE employee_id = ? and payment_kind_id = 2 limit ?,?";
    public static final String SELECT_ALL_FROM_VACATION_BY_EMPL_NUMBER = "Select* from Payment WHERE employee_id = ? and payment_kind_id = 2";
    public static final String SELECT_ALL_FROM_VACATION = " select payment.id, payment.date, payment.sum, private_employee_info.name, payment.comment" +
            " from  payment,  employee,  private_employee_info" +
            " where  payment.employee_id = employee.id and employee.private_employee_info_id = private_employee_info.id and payment_kind_id = 2" +
            " order by payment.id desc";
    public static final String SELECT_INTERVAL_FROM_VACATION = "select payment.id, payment.date, payment.sum, private_employee_info.name, payment.comment" +
            " from  payment,  employee,  private_employee_info" +
            " where  payment.employee_id = employee.id and employee.private_employee_info_id = private_employee_info.id and payment_kind_id = 2" +
            " order by payment.id desc limit ?,?";
}
