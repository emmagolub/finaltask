package com.epam.constants;

/**
 * @author Эмма
 */
public class ResourcesPathes{
    public static final String DB_RESOURCE_LOCATION = "db";
    public static final String EN_MESSAGES_RESOURCE_LOCATION = "en_messages";
    public static final String RU_MESSAGES_RESOURCE_LOCATION = "ru_messages";
    public static final String TAX_RESOURCE_LOCATION = "tax";
    public static final String TAGLIB_RESOURCE_LOCATION = "taglib";
}

