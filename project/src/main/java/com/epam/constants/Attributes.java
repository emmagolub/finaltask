package com.epam.constants;

/**
 * @author Эмма
 */
public class Attributes{
    public static final String ID = "ID";
    public static final String NUMBER = "number";
    public static final String NAME = "name";
    public static final String DEPARTMENT = "department";
    public static final String OCCUPATION = "occupation";
    public static final String DATE = "date";
    public static final String NUMBER_OF_CHILDREN = "numberOfChildren";
    public static final String SINGLE_PARENT = "singleParent";
    public static final String CNPP = "cnpp";
    public static final String EMPLOYEE_LIST = "employeeList";
    public static final String LOGIN = "login";
    public static final String PASSWORD= "password";
    public static final String ACCOUNTS_LIST = "accountList";
    public static final String DISMISSED_LIST = "dismissedList";
    public static final String PERSONAL_NUMBER = "personalNumber";
    public static final String DISMISSING_DATE = "dismissingDate";
    public static final String SALARY = "salary";
    public static final String HOURS_NUMBER = "hoursNumber";
    public static final String HOUR_PAYMENT = "hourPayment";
    public static final String VOCATION_SURCHARGE = "vocationSurcharge";
    public static final String OVERLOAD_SURCHARGE = "overLoadSurcharge";
    public static final String BONUS = "bonus";
    public static final String FINE = "fine";
    public static final String PREPAYMENT = "prepayment";
    public static final String PROF_CONTRIBUTION = "profContribution";
    public static final String SUM = "sum";
    public static final String START_DATE = "startDate";
    public static final String END_DATE = "endDate";
    public static final String SALARY_LIST = "salaryList";
    public static final String PREPAYMENTS_LIST = "prepaymentsList";
    public static final String VACATIONS_LIST = "vacationList";

    public static final String LOGIN_TO_ADD  = "loginToAdd";
    public static final String LOGIN_TO_LOCK = "loginToLock";
    public static final String LOGIN_TO_UNLOCK = "loginToUnlock";
    public static final String TABLE_PAGE_TO_GO = "tablePageToGo";
    public static final String CURRENT_TABLE_PAGE = "currentTablePage";
    public static final String TOTAL_TABLE_PAGES = "totalTablePages";
    public static final String TOTAL_ENTRIES = "totalEntries";
    public static final String EMPLOYEE_NUMBER = "employeeNumber";

    public static final String PAGE = "page";
    public static final String MESSAGE = "message";
}
