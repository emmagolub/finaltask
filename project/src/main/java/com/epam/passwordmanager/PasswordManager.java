package com.epam.passwordmanager;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * This class design for encrypting passwords with MD5 algorythm and salt string.
 * @author Emma
 */
public class PasswordManager {
    /**
     * size of random string, which is added to password for encrypting
     */
    private final int saltSize = 5;
    private static final PasswordManager instance = new PasswordManager();

    private PasswordManager() {
    }

    public static PasswordManager getInstance() {
        return instance;
    }
    /**
     * Compares two passwords by counting hash of incoming password and comparing it with real
     * @param toCheck password to check
     * @param realPassw real password from db
     * @return true if passwords equal, false otherwise
     */
    public boolean comparePasswords(String toCheck, String realPassw) {
        String salt = realPassw.substring(0, 5);
        String saltedCheck = new StringBuilder(salt).append(toCheck).toString();
        String hashCheck = countHash(saltedCheck);
        String saltedHashCheck = new StringBuilder(salt).append(hashCheck).toString();

        boolean result = false;
        if(saltedHashCheck.equals(realPassw)) {
            result = true;
        }
        return result;
    }

    /**
     * Encrypts input string with MD5 algorythm and salt and adds salt to password
     * @param pass input password string to be encoded
     * @return hash string
     */
    public String encryptWithMD5(String pass) {
        String salt = generateSalt();
        String saltedPass = new StringBuilder(salt).append(pass).toString();
        String hash = countHash(saltedPass);
        String output = new StringBuilder(salt).append(hash).toString();

        return output;
    }

    public String countHash(String string) {
        String output = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(string.getBytes());
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            output = sb.toString();
        }
        catch(NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return output;
    }


    /**
     *
     * @return random string as salt
     */
    public String generateSalt() {
        char[] chars = "abcdef0123456789".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for(int i = 0; i < saltSize; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }
}
