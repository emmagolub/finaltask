package com.epam.util;

import com.epam.beans.Salary;
import com.epam.constants.Messages;
import com.epam.constants.ResourcesPathes;
import org.apache.log4j.Logger;

/**
 * Count salary according data from properties file and user data
 * @author Эмма
 */
public class SalaryCounter {
    private static Logger logger = Logger.getLogger(SalaryCounter.class);
    private static float STANDART_PERCENT;
    private static float MIN_AMOUNT;
    private static float MIN_SALARY;
    private static float AMOUNT_PER_CHILD;
    private static float AMOUNT_FOR_SINGLE_PARENT;
    private static float AMOUNT_FOR_AFFECTED_BY_CNPP;
    private static float PENSION_FUND_PERCENT;
    private static PropertiesManager dbResMng;

    static {
        dbResMng = new PropertiesManager(ResourcesPathes.TAX_RESOURCE_LOCATION);
        STANDART_PERCENT = Float.parseFloat(dbResMng.getValue("standart.percent"));
        MIN_AMOUNT = Float.parseFloat(dbResMng.getValue("min.amount"));
        MIN_SALARY = Float.parseFloat(dbResMng.getValue("min.salary"));
        AMOUNT_FOR_AFFECTED_BY_CNPP =Float.parseFloat(dbResMng.getValue("amount.for.affected.by.CNPP"));
        AMOUNT_FOR_SINGLE_PARENT = Float.parseFloat(dbResMng.getValue("amount.for.single.parent"));
        AMOUNT_PER_CHILD = Float.parseFloat(dbResMng.getValue("amount.per.child"));
        PENSION_FUND_PERCENT = Float.parseFloat(dbResMng.getValue("pension.fund"));
    }


    public static void countSalary (Salary salaryData){
        float resultSalary = 0;
        resultSalary += salaryData.getSalary();
        resultSalary += ( salaryData.getHourPayment() * salaryData.getHoursNumber());
        resultSalary += salaryData.getBonus() + salaryData.getOverLoadSurcharge() + salaryData.getVocationSurcharge();
        resultSalary -= (salaryData.getFine() + salaryData.getProfContribution() + salaryData.getPrepayment());

        if (resultSalary <= 0) resultSalary = 0;
        logger.info(Messages.CLEAN_SALARY + resultSalary);


        float incomeTax = resultSalary * STANDART_PERCENT/100.0f;
        if (resultSalary <= MIN_SALARY) {
            incomeTax -= MIN_AMOUNT;
        }

        if (salaryData.getEmployee().getNumberOfChildren() > 0 && salaryData.getEmployee().isSingleParent()){
            incomeTax -= salaryData.getEmployee().getNumberOfChildren() * AMOUNT_PER_CHILD;
        }
        else if (salaryData.getEmployee().getNumberOfChildren() > 0 && !salaryData.getEmployee().isSingleParent()){
            incomeTax -= salaryData.getEmployee().getNumberOfChildren() * AMOUNT_PER_CHILD;
        }

        if (salaryData.getEmployee().isCnpp()) {
            incomeTax -= AMOUNT_FOR_AFFECTED_BY_CNPP;
        }

        float pensionFund = resultSalary * PENSION_FUND_PERCENT/100.0f;
        resultSalary -= pensionFund;
        logger.info(Messages.PENSION_FUND + pensionFund);
        if (incomeTax <= 0) incomeTax = 0;
        logger.info(Messages.SALARY_TAX + incomeTax);

        resultSalary -= incomeTax;
        salaryData.setSum((int)resultSalary);
        logger.info(Messages.RESULT_SALARY + resultSalary);
    }
}
