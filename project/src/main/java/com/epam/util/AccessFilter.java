package com.epam.util;

import com.epam.constants.AccessConstants;
import com.epam.constants.PagePathes;

import javax.servlet.http.HttpServletRequest;

/**
 * This class filters users access to app pages
 */
public class AccessFilter {
    public static boolean isRoleTrue (HttpServletRequest request, String location){
        if((request.getSession() == null || request.getSession().getAttribute(AccessConstants.ROLE) == null) &&
                !location.equals(PagePathes.PATH_TO_LOGIN_PAGE)) {
            return false;
        }
        else if(request.getSession().getAttribute(AccessConstants.ROLE) == AccessConstants.ADMIN_ROLE
                && !location.equals(PagePathes.PATH_TO_ADMIN_PAGE)) {
            return false;
        }
        else if(request.getSession().getAttribute(AccessConstants.ROLE) == AccessConstants.GUEST_ROLE
                && (!location.equals(PagePathes.PATH_TO_LOGIN_PAGE))) {
            return false;
        }
        else if(request.getSession().getAttribute(AccessConstants.ROLE) == AccessConstants.USER_ROLE
                && (location.equals(PagePathes.PATH_TO_ADMIN_PAGE))) {
            return false;
        }
        return true;
    }
}
