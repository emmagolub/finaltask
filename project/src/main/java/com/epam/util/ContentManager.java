package com.epam.util;

import com.epam.constants.ButtonsCommands;
import com.epam.constants.Messages;
import com.epam.constants.PagePathes;
import com.epam.logic.command.commandImpl.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Updates page content when it's updating
 * @author Эмма
 */
public class ContentManager {
    /**
     * According to pageAlias it updates page's content by calling appropriate show commands
     * @param location path to the page
     * @param request request
     */
    public static void addContent(String location, HttpServletRequest request) {
        if(location.equals(PagePathes.PATH_TO_LOGIN_PAGE)){
            Messager.sendMessage(request, Messages.AUTHORIZATION_ERROR);
        }
        if ((location.equals(PagePathes.PATH_TO_ADD_EMPLOYEE_PAGE)
                || location.equals(PagePathes.PATH_TO_ADD_PREPAYMENT_PAGE)
                || location.equals(PagePathes.PATH_TO_ADD_SALARY_PAGE)
                || location.equals(PagePathes.PATH_TO_ADD_VACATION_PAGE)
                || location.equals(PagePathes.PATH_TO_UPDATE_EMPL_PAGE)
                || location.equals(PagePathes.PATH_TO_REMOVE_EMPLOYEE_PAGE)) &&
                !request.getParameter(ButtonsCommands.COMMAND).equals(ButtonsCommands.CHANGE_LANG)){
            Messager.sendMessage(request, Messages.UNCORRECT_INFORMATION);
        }

        if(location.equals(PagePathes.PATH_TO_ALL_EMPLOYEE_PAGE)) {
            new AllEmployeeCmd().execute(request);
        }
        if(location.equals(PagePathes.PATH_TO_DISMISSED_EMPLOYEE_PAGE)) {
            new ShowDismissedCmd().execute(request);
        }
        if(location.equals(PagePathes.PATH_TO_ADMIN_PAGE)){
            new ShowUsersCmd().execute(request);
        }
        if (location.equals(PagePathes.PATH_TO_SALARIES_HISTORY)){
            new SelectSalariesCmd().execute(request);
        }
        if (location.equals(PagePathes.PATH_TO_PREPAYMENTS_HISTORY)){
            new SelectPrepaymentsCmd().execute(request);
        }
        if (location.equals(PagePathes.PATH_TO_VACATIONS_HISTORY)){
            new SelectVacationsCmd().execute(request);
        }
    }
}
