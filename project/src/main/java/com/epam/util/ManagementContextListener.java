package com.epam.util;

import com.epam.connectionspool.ConnectionPool;
import com.epam.exceptions.ConnectionLevelException;
import org.apache.log4j.Logger;

import com.mysql.jdbc.AbandonedConnectionCleanupThread;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
/**
 * This class is listener implementation. It is needed to create
 * <code>ConnectionPool</code> with app start up and close it, when app stops.
 * @author Emma
 */
public class ManagementContextListener implements ServletContextListener {
    private static Logger logger = Logger.getLogger(ManagementContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {
            ConnectionPool.getInstance();
        } catch (ConnectionLevelException e) {
            logger.error(e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        try {
            ConnectionPool.getInstance().closeAll();
        } catch (ConnectionLevelException e) {
            logger.error(e);
        }

        Enumeration<Driver> drivers = DriverManager.getDrivers();
        Driver driver = null;

        while (drivers.hasMoreElements()) {
            try {
                driver = drivers.nextElement();
                DriverManager.deregisterDriver(driver);
            } catch (SQLException e) {
                logger.error(e);
            }
        }

        try {
            AbandonedConnectionCleanupThread.shutdown();
        } catch (InterruptedException e) {
            logger.error(e);
        }
    }
}
