package com.epam.util;

import java.util.ResourceBundle;

/**
 * Created by Эмма on 10.06.2016.
 */
public class PropertiesManager {

    private ResourceBundle bundle;

    public PropertiesManager (String filePath) {
        bundle = ResourceBundle.getBundle(filePath);
    }

    /**
     * This method return property by key
     * @param key a name of property
     * @return value of property
     */
    public String getValue(String key) {
        return bundle.getString(key);
    }
}
