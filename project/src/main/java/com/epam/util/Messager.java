package com.epam.util;

import com.epam.constants.Attributes;
import com.epam.constants.LangConstants;
import com.epam.constants.ResourcesPathes;

import javax.servlet.http.HttpServletRequest;
import java.util.ResourceBundle;

/**
 * This class was created to display messages on the page in different languages
 * Created by Эмма on 29.05.2016.
 */
public class Messager {
    public static void sendMessage(HttpServletRequest request, String message) {
        String lang = request.getSession().getAttribute(LangConstants.BUNDLE).toString();
        ResourceBundle bundle = null;

        if (lang.equals(LangConstants.EN)) {
            bundle = ResourceBundle.getBundle(ResourcesPathes.EN_MESSAGES_RESOURCE_LOCATION);
        }
        if (lang.equals(LangConstants.RU)) {
            bundle = ResourceBundle.getBundle(ResourcesPathes.RU_MESSAGES_RESOURCE_LOCATION);
        }
        request.setAttribute(Attributes.MESSAGE, bundle.getString(message));
    }
}
