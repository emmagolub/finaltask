package com.epam.util;

import com.epam.constants.LangConstants;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * This class implements Session Listener and its designed to add
 * default english bundle attribute to new created session
 * @author Emma
 */
public class ManagerSessionListener implements HttpSessionListener {
    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        httpSessionEvent.getSession().setAttribute(LangConstants.BUNDLE, LangConstants.EN);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {

    }
}
