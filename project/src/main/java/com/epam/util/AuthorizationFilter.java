package com.epam.util;

import com.epam.constants.PagePathes;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This filter class is designed to monitor unauthorized access to web pages.
 *
 * It determines to which pages user with concrete role(or without role) has access.
 * @author Emma
 */
public class AuthorizationFilter implements Filter {
    private static Logger logger = Logger.getLogger(AuthorizationFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
        httpRequest.getRequestDispatcher(PagePathes.PATH_TO_ERROR_PAGE).forward(httpRequest, httpResponse);
    }

    @Override
    public void destroy() {}
}
