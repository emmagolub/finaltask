package com.epam.dao;

import com.epam.exceptions.DaoException;

import java.util.List;

/**
 * Interface with some set of methods, which must correspond to dao pattern
 * to change data between app and db
 * @author Emma
 */
public interface Dao<T> {
    public void insert(T data) throws DaoException;

    public List<T> select(T data) throws DaoException;

    public void delete(T data) throws DaoException;

    public void update(T data) throws DaoException;

    public List <T> select (int startRecordNumber, int recordsNumber) throws DaoException;

    public int getEntriesNumber() throws DaoException;
}
