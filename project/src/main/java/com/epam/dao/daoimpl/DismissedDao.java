package com.epam.dao.daoimpl;

import com.epam.constants.DBTablesFields;
import com.epam.constants.SQLQueryConstants;
import com.epam.dao.Dao;
import com.epam.exceptions.ConnectionLevelException;
import com.epam.exceptions.DaoException;
import com.epam.connectionspool.ConnectionPool;
import com.epam.beans.DismissedEmployee;
import com.epam.constants.ExceptionConstants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class implements the DAO pattern and contains methods that bind
 * the entity <code>DismissedEmployee</code> with the database
 * @author Emma
 */
public class DismissedDao implements Dao<DismissedEmployee> {

    private static DismissedDao instance = new DismissedDao();

    private DismissedDao() {
    }

    public static DismissedDao getInstance() {
        return instance;
    }

    /**
     *
     * @param data includes data to addition
     * @throws DaoException
     */
    @Override
    public void insert(DismissedEmployee data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    @Override
    public ArrayList<DismissedEmployee> select(DismissedEmployee data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(DismissedEmployee dismissedEmployee) throws DaoException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(DismissedEmployee dismissedEmployee) throws DaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQLQueryConstants.UPDATE_EMPLOYEE_STATUS);
            preparedStatement.setString(1, dismissedEmployee.getFinishDate());
            preparedStatement.setInt(2, dismissedEmployee.getNumber());
            preparedStatement.executeUpdate();
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
    }

    /**
     *
     * @param startRecordNumber number of start records number to selection
     * @param recordsNumber number of records to selection
     * @return list of DismissedEmployee to print on the page
     * @throws DaoException
     */
    @Override
    public List<DismissedEmployee> select(int startRecordNumber, int recordsNumber) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ArrayList<DismissedEmployee> dismissedEmployees = new ArrayList<>();
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_INTERVAL_FROM_DISMISSED);
            preparedStatement.setInt(1, startRecordNumber);
            preparedStatement.setInt(2, recordsNumber);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
                DismissedEmployee dismissedEmployee = new DismissedEmployee();
                dismissedEmployee.setNumber(rs.getInt(DBTablesFields.ID));
                dismissedEmployee.setName(rs.getString(DBTablesFields.NAME));
                dismissedEmployee.setStartDate(rs.getString(DBTablesFields.START_DATE));
                dismissedEmployee.setFinishDate(rs.getString(DBTablesFields.FINISH_DATE));
                dismissedEmployee.setDepartment(rs.getString(DBTablesFields.DEPARTMENT));
                dismissedEmployee.setOccupation(rs.getString(DBTablesFields.OCCUPATION));
                dismissedEmployees.add(dismissedEmployee);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return dismissedEmployees;
    }

    /**
     * Count entries number in the table
     * @return entries number
     * @throws DaoException
     */
    @Override
    public int getEntriesNumber() throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        int entriesNumber = 0;
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_ALL_FROM_DISMISSED);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
               ++entriesNumber;
            }
        }
        catch(SQLException e){
            e.printStackTrace();
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return entriesNumber;
    }
}
