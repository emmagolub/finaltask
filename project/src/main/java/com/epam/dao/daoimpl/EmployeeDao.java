package com.epam.dao.daoimpl;

import com.epam.beans.Employee;
import com.epam.connectionspool.ConnectionPool;
import com.epam.constants.DBTablesFields;
import com.epam.constants.ExceptionConstants;
import com.epam.constants.SQLQueryConstants;
import com.epam.dao.Dao;
import com.epam.exceptions.ConnectionLevelException;
import com.epam.exceptions.DaoException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This class implements the DAO pattern and contains methods that bind
 * the entity <code>Employee</code> with the database
 * @author Emma
 */
public class EmployeeDao implements Dao<Employee> {
    private static EmployeeDao instance = new EmployeeDao();

    private EmployeeDao() {
    }

    public static EmployeeDao getInstance() {
        return instance;
    }

    /**
     * @param data includes information about employee to addition in DB
     * @throws DaoException
     */
    @Override
    public void insert(Employee data) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            preparedStatement = connection.prepareStatement(SQLQueryConstants.INSERT_INTO_EMPLOYEE_INFO);
            preparedStatement.setString(1, data.getDepartment());
            preparedStatement.setString(2, data.getOccupation());
            preparedStatement.setString(3, data.getDate());
            preparedStatement.executeUpdate();

            preparedStatement = connection.prepareStatement(SQLQueryConstants.INSERT_INTO_PRIVATE_EMPLOYEE_INFO);
            preparedStatement.setString(1, data.getName());
            preparedStatement.setBoolean(2, data.isSingleParent());
            preparedStatement.setBoolean(3, data.isCnpp());
            preparedStatement.setInt(4, data.getNumberOfChildren());
            preparedStatement.executeUpdate();

            preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_EMPLOYEE_NUMBER);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                preparedStatement = connection.prepareStatement(SQLQueryConstants.INSERT_INTO_EMPLOYEE);
                int employeeId = rs.getInt(DBTablesFields.ID) + 1;
                System.out.println(employeeId);

                preparedStatement.setInt(1, employeeId);
                preparedStatement.setInt(2, employeeId);
                preparedStatement.executeUpdate();
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }

    }

    @Override
    public ArrayList<Employee> select(Employee data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param startRecordNumber number of start records number to selection
     * @param recordsNumber number of records to selection
     * @return list of employees to print on the page
     * @throws DaoException
     */
    @Override
    public List <Employee> select(int startRecordNumber, int recordsNumber) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        List <Employee> employees = new ArrayList<>();

        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_INTERVAL_FROM_EMPLOYEE);
            preparedStatement.setInt(1, startRecordNumber);
            preparedStatement.setInt(2, recordsNumber);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
                Employee employee = new Employee();
                employee.setNumber(rs.getInt(DBTablesFields.ID));
                employee.setName(rs.getString(DBTablesFields.NAME));
                employee.setDate(rs.getString(DBTablesFields.START_DATE));
                employee.setDepartment(rs.getString(DBTablesFields.DEPARTMENT));
                employee.setOccupation(rs.getString(DBTablesFields.OCCUPATION));
                employee.setNumberOfChildren(rs.getInt(DBTablesFields.NUMBER_OF_CHILDREN));
                employee.setSingleParent(rs.getBoolean(DBTablesFields.SINGLE_PARENT));
                employee.setCnpp(rs.getBoolean(DBTablesFields.AFFECTED_BY_CNPP));
                employees.add(employee);
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return employees;
    }

    /**
     * Count entries number in the table
     * @return entries number
     * @throws DaoException
     */
    @Override
    public int getEntriesNumber() throws DaoException{
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        int entriesNumber = 0;

        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_ALL_FROM_EMPLOYEE);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
                ++entriesNumber;
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return entriesNumber;
    }

    /**
     *
     * @param number select an employee tby his number
     * @return employee entity to show
     * @throws DaoException
     */
    public Employee select(int number) throws DaoException {
        PreparedStatement preparedStatement;
        Connection connection = null;
        Employee employee = null;
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_FROM_EMPLOYEE_BY_NUMBER);
            preparedStatement.setInt(1, number);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
                employee = new Employee();
                employee.setName(rs.getString(DBTablesFields.NAME));
                employee.setDate(rs.getString(DBTablesFields.START_DATE));
                employee.setDepartment(rs.getString(DBTablesFields.DEPARTMENT));
                employee.setOccupation(rs.getString(DBTablesFields.OCCUPATION));
                employee.setNumberOfChildren(rs.getInt(DBTablesFields.NUMBER_OF_CHILDREN));
                employee.setSingleParent(rs.getBoolean(DBTablesFields.SINGLE_PARENT));
                employee.setCnpp(rs.getBoolean(DBTablesFields.AFFECTED_BY_CNPP));
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return employee;
    }


    @Override
    public void delete(Employee data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param data includes data to updating employee
     * @throws DaoException
     */
    @Override
    public void update(Employee data) throws DaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQLQueryConstants.UPDATE_PRIVATE_EMPLOYEE_INFO);
            preparedStatement.setString(1, data.getName());
            preparedStatement.setBoolean(2, data.isSingleParent());
            preparedStatement.setBoolean(3, data.isCnpp());
            preparedStatement.setInt(4, data.getNumberOfChildren());
            preparedStatement.setInt(5, data.getNumber());
            preparedStatement.executeUpdate();

            preparedStatement = connection.prepareStatement(SQLQueryConstants.UPDATE_EMPLOYEE_INFO);
            preparedStatement.setDate(1, Date.valueOf(data.getDate()));
            preparedStatement.setString(2, data.getDepartment());
            preparedStatement.setString(3, data.getOccupation());
            preparedStatement.setInt(4, data.getNumber());
            preparedStatement.executeUpdate();
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
    }
}
