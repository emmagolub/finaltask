package com.epam.dao.daoimpl;


import com.epam.beans.Account;
import com.epam.connectionspool.ConnectionPool;
import com.epam.constants.DBTablesFields;
import com.epam.constants.ExceptionConstants;
import com.epam.constants.SQLQueryConstants;
import com.epam.dao.Dao;
import com.epam.exceptions.ConnectionLevelException;
import com.epam.exceptions.DaoException;
import com.epam.passwordmanager.PasswordManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class implements the DAO pattern and contains methods that bind
 * the entity <code>Account</code> with the database
 * @author Emma
 */
public class AuthorizationDao implements Dao<Account> {
    private static AuthorizationDao instance = new AuthorizationDao();

    private AuthorizationDao() {
    }

    public static AuthorizationDao getInstance() {
        return instance;
    }

    /**
     * Adds Account entity to database
     * @param data entity to be added
     * @throws DaoException
     */
    public void insert(Account data) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            preparedStatement = connection.prepareStatement(SQLQueryConstants.INSERT_AUTHORIZATION);
            PasswordManager passwordManager = PasswordManager.getInstance();
            preparedStatement.setString(1, data.getLogin());

            preparedStatement.setString(4, passwordManager.encryptWithMD5(data.getPassword()));
            preparedStatement.setInt(2, data.getRoleId());
            preparedStatement.setInt(3, data.getStatusId());
            preparedStatement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }  catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
    }

    public static void main(String[] args) {
        AuthorizationDao authorizationDao = AuthorizationDao.getInstance();
        try {
            authorizationDao.insert(new Account("admin", "admin", 1, 1));
            authorizationDao.insert(new Account("user", "user", 2, 1));
            authorizationDao.insert(new Account("user1", "user1", 2, 1));
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }


    /**
     *
     * @param data includes user login to select information
     * @return account entities
     * @throws DaoException
     */
   public List<Account> select(Account data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    public Account select(String login) throws DaoException {
        PreparedStatement preparedStatement = null;
        Account account = null;
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_BY_LOGIN_FROM_AUTHORIZATION);
            preparedStatement.setString(1, login);

            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                account = new Account();
                account.setLogin(rs.getString(DBTablesFields.LOGIN));
                account.setPassword(rs.getString(DBTablesFields.PASSWORD));
                account.setRoleId(rs.getInt(DBTablesFields.ROLE));
                account.setStatusId(rs.getInt(DBTablesFields.STATUS));
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            e.printStackTrace();
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return account;
    }

    /**
     * @return Account entities
     * @throws DaoException
     */
    public List<Account> select() throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ArrayList<Account> accounts = new ArrayList<>();

        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_ALL_FROM_AUTHORIZATION);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
                Account account = new Account();
                account.setLogin(rs.getString(DBTablesFields.LOGIN));
                account.setRoleName(rs.getString(DBTablesFields.ROLE_NAME));
                account.setStatusName(rs.getString(DBTablesFields.STATUS_NAME));
                accounts.add(account);
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return accounts;
    }

    public void delete(Account data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    /**
     * Updates information in DB by account login
     * @param data includes user login to update
     * @throws DaoException
     */
    public void update(Account data) throws DaoException {
        Connection connection = null;
        try {
            PreparedStatement preparedStatement = null;
            connection = ConnectionPool.getInstance().getFreeConnection();
            preparedStatement = connection.prepareStatement(SQLQueryConstants.UPDATE_AUTHORIZATION);
            preparedStatement.setInt(1, data.getStatusId());
            preparedStatement.setString(2, data.getLogin());

            preparedStatement.executeUpdate();
        }
        catch(SQLException err) {
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
    }

    @Override
    public List<Account> select(int startRecordNumber, int recordsNumber) throws DaoException {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getEntriesNumber() throws DaoException {
        throw new UnsupportedOperationException();
    }
}
