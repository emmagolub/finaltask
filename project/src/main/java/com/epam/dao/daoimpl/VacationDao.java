package com.epam.dao.daoimpl;

import com.epam.beans.Employee;
import com.epam.beans.Vacation;
import com.epam.connectionspool.ConnectionPool;
import com.epam.constants.DBTablesFields;
import com.epam.constants.ExceptionConstants;
import com.epam.constants.SQLQueryConstants;
import com.epam.dao.Dao;
import com.epam.exceptions.ConnectionLevelException;
import com.epam.exceptions.DaoException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class implements the DAO pattern and contains methods that bind
 * the entity <code>Vacation</code> with the database
 * @author Emma
 */
public class VacationDao implements Dao<Vacation> {

    private static VacationDao instance = new VacationDao();
    private VacationDao (){}

    public static VacationDao getInstance (){return instance;}

    /**
     *
     * @param data includes information to addition
     * @throws DaoException
     */
    @Override
    public void insert(Vacation data) throws DaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            List<Vacation> dbData = select(data);
            int repeatCounter = 0;
            for(Vacation vacation: dbData){
                if(vacation.getDate().equals(data.getDate()))
                    repeatCounter++;
            }
            if(repeatCounter >= 3) throw new DaoException(ExceptionConstants.VACATION);
            else {
                PreparedStatement preparedStatement = connection.prepareStatement(SQLQueryConstants.INSERT_INTO_VACATION);
                preparedStatement.setFloat(1, data.getEmployee().getNumber());
                preparedStatement.setDate(2, data.getDate());
                preparedStatement.setFloat(3, data.getSum());
                preparedStatement.setString(4, data.getPeriod());
                preparedStatement.executeUpdate();
            }
        }
        catch(SQLException e) {
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
    }

    @Override
    public List<Vacation> select(Vacation data) throws DaoException {
        Connection connection = null;
        List<Vacation> vacations = new ArrayList<>();
        PreparedStatement preparedStatement = null;

        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_ALL_FROM_VACATION_BY_EMPL_NUMBER);
            preparedStatement.setInt(1, data.getEmployee().getNumber());
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
                Vacation vacation = new Vacation();
                vacation.setPeriod(rs.getString(DBTablesFields.COMMENT));
                vacation.setDate(rs.getDate(DBTablesFields.DATE));
                vacation.setSum(rs.getInt(DBTablesFields.SUM));
                vacations.add(vacation);
            }
        }
        catch(SQLException e) {
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }

        return vacations;
    }


    /**
     *
     * @return salaries list
     * @throws DaoException
     */
    public List<Vacation> select() throws DaoException {
        PreparedStatement preparedStatement = null;
        List<Vacation> vacations = new ArrayList<>();
        Connection connection = null;

        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_ALL_FROM_VACATION);

            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next()) {
                Employee employee = new Employee();
                Vacation vacation = new Vacation();
                vacation.setId(rs.getInt(DBTablesFields.ID));
                employee.setName(rs.getString(DBTablesFields.NAME));
                vacation.setEmployee(employee);
                vacation.setSum(rs.getInt(DBTablesFields.SUM));
                vacation.setDate(rs.getDate(DBTablesFields.DATE));
                vacation.setPeriod(rs.getString(DBTablesFields.COMMENT));
                vacations.add(vacation);
            }
        }
        catch(SQLException e) {
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return vacations;
    }

    @Override
    public void delete(Vacation data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(Vacation data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param startRecordNumber number of start records number to selection
     * @param recordsNumber number of records to selection
     * @return list of employees to print on the page
     * @throws DaoException
     */
    @Override
    public List<Vacation> select(int startRecordNumber, int recordsNumber) throws DaoException {
        PreparedStatement preparedStatement = null;
        List<Vacation> vacations = new ArrayList<>();
        Connection connection = null;

        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_INTERVAL_FROM_VACATION);
            preparedStatement.setInt(1, startRecordNumber);
            preparedStatement.setInt(2, recordsNumber);

            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next()) {
                Employee employee = new Employee();
                Vacation vacation = new Vacation();
                vacation.setId(rs.getInt(DBTablesFields.ID));
                employee.setName(rs.getString(DBTablesFields.NAME));
                vacation.setEmployee(employee);
                vacation.setSum(rs.getInt(DBTablesFields.SUM));
                vacation.setDate(rs.getDate(DBTablesFields.DATE));
                vacation.setPeriod(rs.getString(DBTablesFields.COMMENT));
                vacations.add(vacation);
            }
        }
        catch(SQLException e) {
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return vacations;
    }


    /**
     *
     * @param data employee number to select
     * @param startRecordNumber number of start records number to selection
     * @param recordsNumber number of records to selection
     * @return list of employees to print on the page
     * @throws DaoException
     */
    public List<Vacation> select(Vacation data, int startRecordNumber, int recordsNumber) throws DaoException {
        List<Vacation> vacations = new ArrayList<>();
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            PreparedStatement preparedStatement = connection
                    .prepareStatement(SQLQueryConstants.SELECT_FROM_VACATION_BY_EMPL_NUMBER);
            preparedStatement.setInt(1, data.getEmployee().getNumber());
            preparedStatement.setInt(2, startRecordNumber);
            preparedStatement.setInt(3, recordsNumber);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
                Vacation vacation = new Vacation();
                vacation.setId(rs.getInt(DBTablesFields.ID));
                vacation.setSum(rs.getInt(DBTablesFields.SUM));
                vacation.setDate(rs.getDate(DBTablesFields.DATE));
                vacation.setPeriod(rs.getString(DBTablesFields.COMMENT));
                vacations.add(vacation);
            }
        }
        catch(SQLException e) {
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return vacations;
    }

    /**
     * Count entries number in the table
     * @return entries number
     * @throws DaoException
     */
    @Override
    public int getEntriesNumber() throws DaoException {
        PreparedStatement preparedStatement = null;
        List<Vacation> vacations = new ArrayList<>();
        Connection connection = null;
        int recordsNumber = 0;

        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_ALL_FROM_VACATION);

            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next()) {
                ++recordsNumber;
            }
        }
        catch(SQLException e) {
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return recordsNumber;
    }


    /**
     * Count entries number in the table
     * @param employeeNumber employee number to count entries with him
     * @return entries number
     * @throws DaoException
     */
    public int getEntriesNumber(int employeeNumber) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        int recordsCounter = 0;
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_ALL_FROM_VACATION_BY_EMPL_NUMBER);
            preparedStatement.setInt(1, employeeNumber);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
                ++recordsCounter;
            }
        }
        catch(SQLException e) {
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return recordsCounter;
    }
}
