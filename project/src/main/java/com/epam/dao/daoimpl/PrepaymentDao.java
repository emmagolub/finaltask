package com.epam.dao.daoimpl;

import com.epam.beans.Employee;
import com.epam.beans.Prepayment;
import com.epam.connectionspool.ConnectionPool;
import com.epam.constants.DBTablesFields;
import com.epam.constants.ExceptionConstants;
import com.epam.constants.SQLQueryConstants;
import com.epam.dao.Dao;
import com.epam.exceptions.ConnectionLevelException;
import com.epam.exceptions.DaoException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class implements the DAO pattern and contains methods that bind
 * the entity <code>Prepayment</code> with the database
 * @author Emma
 */
public class PrepaymentDao implements Dao<Prepayment> {
    private static PrepaymentDao instance = new PrepaymentDao();
    private PrepaymentDao (){}

    public static PrepaymentDao getInstance (){return instance;}

    /**
     *
     * @param data includes information to add
     * @throws DaoException
     */
    @Override
    public void insert(Prepayment data) throws DaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQLQueryConstants.INSERT_INTO_PREPAYMENT);
            preparedStatement.setInt(1, data.getEmployee().getNumber());
            preparedStatement.setDate(2, data.getDate());
            preparedStatement.setFloat(3, data.getSum());
            preparedStatement.executeUpdate();
        }catch(SQLException e) {
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
    }

    /**
     *
     * @param data includes prepayment number to select by it
     * @return prepayments list
     * @throws DaoException
     */
    @Override
    public List <Prepayment> select(Prepayment data) throws DaoException {
        Connection connection = null;
        List <Prepayment> prepayments = new ArrayList<>();

        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            PreparedStatement preparedStatement = connection
                    .prepareStatement(SQLQueryConstants.SELECT_BY_EMPL_NUMBER_FROM_PREPAYMENT);
            preparedStatement.setInt(1, data.getEmployee().getNumber());
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
                Prepayment prepayment = new Prepayment();
                prepayment.setDate(rs.getDate(DBTablesFields.DATE));
                prepayment.setSum(rs.getInt(DBTablesFields.SUM));
                prepayments.add(prepayment);

            }
        }catch(SQLException e) {
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return prepayments;
    }

    /**
     *
     * @return prepayments list
     * @throws DaoException
     */
    public List <Prepayment> select() throws DaoException {
        Connection connection = null;
        List<Prepayment> prepayments = new ArrayList<>();
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_ALL_FROM_PREPAYMENT);

            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next()) {
                Prepayment prepayment = new Prepayment();
                prepayment.setId(rs.getInt(DBTablesFields.ID));
                Employee employee = new Employee();
                employee.setName(rs.getString(DBTablesFields.NAME));
                prepayment.setEmployee(employee);
                prepayment.setSum(rs.getInt(DBTablesFields.SUM));
                prepayment.setDate(rs.getDate(DBTablesFields.DATE));
                prepayments.add(prepayment);
            }
        }
        catch(SQLException e) {
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return prepayments;
    }

    @Override
    public void delete(Prepayment data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(Prepayment data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param startRecordNumber number of start records number to selection
     * @param recordsNumber number of records to selection
     * @return list of employees to print on the page
     * @throws DaoException
     */
    @Override
    public List<Prepayment> select(int startRecordNumber, int recordsNumber) throws DaoException {
        Connection connection = null;
        List<Prepayment> prepayments = new ArrayList<>();
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_INTERVAL_FROM_PREPAYMENT);
            preparedStatement.setInt(1, startRecordNumber);
            preparedStatement.setInt(2,recordsNumber);

            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next()) {
                Prepayment prepayment = new Prepayment();
                prepayment.setId(rs.getInt(DBTablesFields.ID));
                Employee employee = new Employee();
                employee.setName(rs.getString(DBTablesFields.NAME));
                prepayment.setEmployee(employee);
                prepayment.setSum(rs.getInt(DBTablesFields.SUM));
                prepayment.setDate(rs.getDate(DBTablesFields.DATE));
                prepayments.add(prepayment);
            }
        }
        catch(SQLException e) {
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return prepayments;
    }

    /**
     *
     * @param data employee number to select
     * @param startRecordNumber number of start records number to selection
     * @param recordsNumber number of records to selection
     * @return list of employees to print on the page
     * @throws DaoException
     */
    public List<Prepayment> select(Prepayment data, int startRecordNumber, int recordsNumber) throws DaoException {
        Connection connection = null;
        List<Prepayment> prepayments = new ArrayList<>();
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_BY_EMPL_NUMBER_FROM_PREPAYMENT);
            preparedStatement.setInt(1, data.getEmployee().getNumber());
            preparedStatement.setInt(2, startRecordNumber);
            preparedStatement.setInt(3,recordsNumber);

            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next()) {
                Prepayment prepayment = new Prepayment();
                prepayment.setId(rs.getInt(DBTablesFields.ID));
                prepayment.setSum(rs.getInt(DBTablesFields.SUM));
                prepayment.setDate(rs.getDate(DBTablesFields.DATE));
                prepayments.add(prepayment);
            }
        }
        catch(SQLException e) {
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return prepayments;
    }

    /**
     * Count entries number in the table
     * @return entries number
     * @throws DaoException
     */
    @Override
    public int getEntriesNumber() throws DaoException {
        Connection connection = null;
        int recordsNumber = 0;
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_ALL_FROM_PREPAYMENT);

            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next()) {
                ++recordsNumber;
            }
        }
        catch(SQLException e) {
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return recordsNumber;
    }

    /**
     * Count entries number in the table
     * @param employeeNumber employee number to count entries with him
     * @return entries number
     * @throws DaoException
     */
    public int getEntriesNumber(int employeeNumber) throws DaoException {
        Connection connection = null;
        List<Prepayment> prepayments = new ArrayList<>();
        int recordsNumber = 0;
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_ALL_FROM_PREPAYMENT_BY_EMPL_NUMBER);
            preparedStatement.setInt(1, employeeNumber);

            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next()) {
                ++recordsNumber;
            }
        }
        catch(SQLException e) {
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return recordsNumber;
    }
}
