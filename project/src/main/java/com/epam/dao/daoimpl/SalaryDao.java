package com.epam.dao.daoimpl;

import com.epam.constants.DBTablesFields;
import com.epam.constants.SQLQueryConstants;
import com.epam.dao.Dao;
import com.epam.exceptions.ConnectionLevelException;
import com.epam.exceptions.DaoException;
import com.epam.beans.Employee;
import com.epam.beans.Salary;
import com.epam.connectionspool.ConnectionPool;
import com.epam.constants.ExceptionConstants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
/**
 * This class implements the DAO pattern and contains methods that bind
 * the entity <code>Salary</code> with the database
 * @author Emma
 */
public class SalaryDao implements Dao<Salary> {

    private static SalaryDao instance = new SalaryDao();
    private SalaryDao (){}

    public static  SalaryDao getInstance (){return instance;}

    /**
     *
     * @param data includes information to addition
     * @throws DaoException
     */
    @Override
    public void insert(Salary data) throws DaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();

            PreparedStatement preparedStatement = null;
            preparedStatement = connection.prepareStatement(SQLQueryConstants.INSERT_INTO_SALARY);

            preparedStatement.setInt(1, data.getEmployee().getNumber());
            preparedStatement.setDate(2, data.getDate());
            preparedStatement.setFloat(3, data.getSum());

            preparedStatement.executeUpdate();
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }

    }

    @Override
    public List<Salary> select(Salary data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param data employee number to select
     * @param startRecordNumber number of start records number to selection
     * @param recordsNumber number of records to selection
     * @return list of employees to print on the page
     * @throws DaoException
     */
    public List<Salary> select(Salary data, int startRecordNumber, int recordsNumber) throws DaoException {
        List<Salary> salaries = new ArrayList<>();
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            PreparedStatement preparedStatement = connection
                    .prepareStatement(SQLQueryConstants.SELECT_FROM_SALARY_BY_EMPL_NUMBER);
            preparedStatement.setInt(1, data.getEmployee().getNumber());
            preparedStatement.setInt(2, startRecordNumber);
            preparedStatement.setInt(3, recordsNumber);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
                Salary salary = new Salary();
                salary.setId(rs.getInt(DBTablesFields.ID));
                salary.setDate(rs.getDate(DBTablesFields.DATE));
                salary.setSum(rs.getInt(DBTablesFields.SUM));
                salaries.add(salary);
            }
        }
        catch(SQLException e) {
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return salaries;
    }

    /**
     *
     * @return salaries list
     * @throws DaoException
     */
    public List<Salary> select() throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        List <Salary> salaries = new ArrayList<>();
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_ALL_FROM_SALARY);

            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
                Salary salary = new Salary();
                salary.setId(rs.getInt(DBTablesFields.SALARY_ID));
                Employee employee = new Employee();
                employee.setName(rs.getString(DBTablesFields.EMPLOYEE_NAME));
                salary.setEmployee(employee);
                salary.setSum(rs.getInt(DBTablesFields.SALARY_SUM));
                salary.setDate(rs.getDate(DBTablesFields.SALARY_DATE));
                salaries.add(salary);
            }
        }
        catch(SQLException e) {
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return salaries;
    }

    @Override
    public void delete(Salary data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(Salary data) throws DaoException {
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param startRecordNumber number of start records number to selection
     * @param recordsNumber number of records to selection
     * @return list of employees to print on the page
     * @throws DaoException
     */
    @Override
    public List<Salary> select(int startRecordNumber, int recordsNumber) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        List <Salary> salaries = new ArrayList<>();
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_INTERVAL_FROM_SALARY);
            preparedStatement.setInt(1, startRecordNumber);
            preparedStatement.setInt(2, recordsNumber);

            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
                Salary salary = new Salary();
                salary.setId(rs.getInt(DBTablesFields.SALARY_ID));
                Employee employee = new Employee();
                employee.setName(rs.getString(DBTablesFields.NAME));
                salary.setEmployee(employee);
                salary.setSum(rs.getInt(DBTablesFields.SUM));
                salary.setDate(rs.getDate(DBTablesFields.DATE));
                salaries.add(salary);
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return salaries;
    }

    /**
     * Count entries number in the table
     * @return entries number
     * @throws DaoException
     */
    @Override
    public int getEntriesNumber() throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        List <Salary> salaries = new ArrayList<>();
        int recordsCounter = 0;
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_ALL_FROM_SALARY);

            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
               ++recordsCounter;
            }
        }
        catch(SQLException e) {
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return recordsCounter;
    }

    /**
     * Count entries number in the table
     * @param employeeNumber employee number to count entries with him
     * @return entries number
     * @throws DaoException
     */
    public int getEntriesNumber(int employeeNumber) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        int recordsCounter = 0;
        try {
            connection = ConnectionPool.getInstance().getFreeConnection();
            preparedStatement = connection.prepareStatement(SQLQueryConstants.SELECT_ALL_FROM_SALARY_BY_EMPL_NUMBER);
            preparedStatement.setInt(1, employeeNumber);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
                ++recordsCounter;
            }
        }
        catch(SQLException e) {
            throw new DaoException(ExceptionConstants.SQL_FAIL);
        }
        catch(ConnectionLevelException e) {
            throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
        }
        finally {
            try {
                ConnectionPool.getInstance().freeConnection(connection);
            }
            catch(ConnectionLevelException e) {
                throw new DaoException(ExceptionConstants.CONNECTION_FAIL);
            }
        }
        return recordsCounter;
    }
}
