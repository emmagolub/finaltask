package com.epam.tags;


import com.epam.constants.ResourcesPathes;
import com.epam.util.PropertiesManager;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * This tag outputs the collection an object splitting it on pages.
 * It also outputs menu to navigate between pages and show total entries count
 *
 * @author Emma
 */
public class PaginationTag extends SimpleTagSupport {
    private String action;
    private String command;
    private String pStyleClass;
    private String btnClass;
    private int current;
    private int totalPages;
    private int totalEntries;
    private PropertiesManager propertiesManager =  new PropertiesManager(ResourcesPathes.TAGLIB_RESOURCE_LOCATION);
    

    private String DIV_START = "pagination.divStartTag";
    private String FORM_START_BEGIN = "pagination.formStartTagBegin";
    private String FORM_START_END = "pagination.formStartTagEnd";
    private String P_START_BEGIN = "pagination.pStartTagBegin";
    private String P_START_END = "pagination.pStartTagEnd";
    private String CMD_BEGIN = "pagination.cmdInputBegin";
    private String CMD_END = "pagination.cmdInputEnd";
    private String INP_START_CLASS = "pagination.inptStartToClass";
    private String INP_CLASS_VAL = "pagination.inptClassToVal";
    private String INP_END = "pagination.inptEnd";
    private String P_END = "pagination.pEndTag";
    private String FORM_END = "pagination.formEndTag";
    private String DIV_END = "pagination.divEndTag";


    public void setAction(String action) {
        this.action = action;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public void setpStyleClass(String pStyleClass) {
        this.pStyleClass = pStyleClass;
    }

    public void setBtnClass(String btnClass) {
        this.btnClass = btnClass;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public void setTotalEntries(int totalEntries) {
        this.totalEntries = totalEntries;
    }

    @Override
    public void doTag() throws JspException, IOException {
        StringBuilder sb = new StringBuilder(propertiesManager.getValue(DIV_START));
        sb.append(propertiesManager.getValue(FORM_START_BEGIN)).append(action).append(propertiesManager.getValue(FORM_START_END));
        sb.append(propertiesManager.getValue(P_START_BEGIN)).append(pStyleClass).append(propertiesManager.getValue(P_START_END));
        sb.append(propertiesManager.getValue(CMD_BEGIN)).append(command).append(propertiesManager.getValue(CMD_END));


        for (int i = 0; i < totalPages; i++) {
            if (current == i + 1) {
                sb.append("&nbsp;").append(i + 1).append("&nbsp;");
            } else {
                sb.append(propertiesManager.getValue(INP_START_CLASS)).append(btnClass)
                        .append(propertiesManager.getValue(INP_CLASS_VAL)).append(i + 1).append(propertiesManager.getValue(INP_END))
                        .append("&nbsp;");
            }
        }
        sb.append(propertiesManager.getValue(P_END));
        sb.append(propertiesManager.getValue(P_START_BEGIN)).append(pStyleClass).append(propertiesManager.getValue(P_START_END));
        sb.append("Total: ").append(totalEntries);
        sb.append(propertiesManager.getValue(P_END)).append(propertiesManager.getValue(FORM_END)).append(propertiesManager.getValue(DIV_END));

        getJspContext().getOut().write(sb.toString());
    }
}
