<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setBundle basename="${bundle}" var="lang"/>
    <title><fmt:message key="title.authorization" bundle="${lang}"/></title>
    <title>Авторизация</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/css.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/locale-buttons.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/login.css"/>
</head>

<body>

<div id="picture-div">

    <div id="header">
        <div id="logo-wrap"><img src="${pageContext.request.contextPath}/img/logo.png">
            <div align="rigth">
                <form name="changeLangForm" action="Control" method="get">
                    <input type="hidden" name="cmd" value="changeLang">
                    <input type="hidden" name="path" value="login.jsp">
                    <input type="submit" class="locale-btn" name="langBtn" value="ru">
                    <input type="submit" class="locale-btn" name="langBtn" value="en">
                </form>
            </div>
        </div>
    </div>

    <div id="form-wrap">

        <form id="login-form" action="${pageContext.request.contextPath}/Control" method="post">
            <h3 id="form-title"><fmt:message key="form.title.authorization" bundle="${lang}"/></h3>

            <p id="message">${message}</p>
            <input class="form-input" type="text" required="true" placeholder="<fmt:message key="input.login" bundle="${lang}"/>" id="login" name="login"/>
            <input class="form-input" type="password" required="true" placeholder="<fmt:message key="input.password" bundle="${lang}"/>" id="password"
                   name="password"/>
            <input type="submit" class="submit-btn" value="<fmt:message key="button.login" bundle="${lang}"/>"/>
            <input type="hidden" name="cmd" value="login">
        </form>
    </div>
</div>


</body>
</html>