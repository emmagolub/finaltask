<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="aTag" uri="/WEB-INF/taglib.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
    <fmt:setBundle basename="${bundle}" var="lang"/>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/info-table.css">
    <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/buttons.css">
    <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/history.css">
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/hamburger-menu.js"></script>

</head>
<title><fmt:message key="title.all.employees" bundle="${lang}"/></title>

<body>

<div id="pgcontainer">
    <header>
        <div id="navbar">
            <a href="#" class="menubtn">menu</a>

            <div id="hamburgermenu">
                <ul>
                    <li>
                        <form name="changeLangForm" action="Control" method="get">
                            <input type="hidden" name="cmd" value="changeLang">
                            <input type="hidden" name="path" value="html/allEmployee.jsp">
                            <input type="submit" class="locale-btn" name="langBtn" value="ru_Ru">
                            <input type="submit" class="locale-btn" name="langBtn" value="en_En">
                        </form>
                    </li>
                    <jsp:include page="menu.jsp"/>

                </ul>
            </div>
        </div>
        <div class="overlay"></div>
    </header>

    <div id="content">
        <form id="otp-form">

            <div id="wrap-form">
                <h1><fmt:message key="title.all.employees" bundle="${lang}"/></h1>
                <hr>
                <h2></h2>
                <p id="message">${message}</p>

                <div class="myTable">
                    <table>
                        <tr>
                            <td><fmt:message key="td.number" bundle="${lang}"/></td>
                            <td><fmt:message key="td.name" bundle="${lang}"/></td>
                            <td><fmt:message key="td.start.date" bundle="${lang}"/></td>
                            <td><fmt:message key="td.department" bundle="${lang}"/></td>
                            <td><fmt:message key="td.occupation" bundle="${lang}"/></td>
                            <td><fmt:message key="input.children.number" bundle="${lang}"/></td>
                            <td><fmt:message key="td.single.parent" bundle="${lang}"/></td>
                            <td><fmt:message key="td.cnpp" bundle="${lang}"/></td>
                        </tr>
                        <c:forEach var="employee" items="${employeeList}">
                            <tr>
                                <td><c:out value="${employee.number}"/></td>
                                <td><c:out value="${employee.name}"/></td>
                                <td><c:out value="${employee.date}"/></td>
                                <td><c:out value="${employee.department}"/></td>
                                <td><c:out value="${employee.occupation}"/></td>
                                <td><c:out value="${employee.numberOfChildren}"/></td>

                                <td>
                                    <c:if test="${employee.singleParent == true}">
                                        <fmt:message key="yes" bundle="${lang}"/>
                                    </c:if>
                                    <c:if test="${employee.singleParent == false}">
                                        <fmt:message key="no" bundle="${lang}"/>
                                    </c:if>
                                </td>
                                <td>
                                    <c:if test="${employee.cnpp == true}">
                                        <fmt:message key="yes" bundle="${lang}"/>
                                    </c:if>
                                    <c:if test="${employee.cnpp == false}">
                                        <fmt:message key="no" bundle="${lang}"/>
                                    </c:if>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
            <aTag:pagination action="Control" command="showEmployee"
                             totalEntries="${totalEntries}" current="${currentTablePage}"
                             totalPages="${totalTablePages}"
                             pStyleClass="paginationP" btnClass="pagination-btn"/>
        </form>

    </div>
</div>

</body>
