<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
  <fmt:setBundle basename="${bundle}" var="lang"/>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/info-table.css">
  <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/buttons.css">
  <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/styles.css">
  <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/history.css">
  <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/input.css">
  <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/js/hamburger-menu.js"></script>

</head>
<title><fmt:message key="title.search" bundle="${lang}"/></title>

<body>

<div id="pgcontainer">
  <header>
    <div id="navbar">
      <a href="#" class="menubtn">menu</a>

      <div id="hamburgermenu">
        <ul>
          <li>
            <form name="changeLangForm" action="Control" method="get">
              <input type="hidden" name="cmd" value="changeLang">
              <input type="hidden" name="path" value="html/vacationsSearch.jsp">
              <input type="submit" class="locale-btn" name="langBtn" value="ru_Ru">
              <input type="submit" class="locale-btn" name="langBtn" value="en_En">
            </form>
          </li>
          <jsp:include page="menu.jsp"/>

        </ul>
      </div>
    </div>
    <div class="overlay"></div>
  </header>

  <div id="content">
    <form id="otp-form" action="${pageContext.request.contextPath}/Control" method="get">
      <div class="wrap-form">
        <h1><fmt:message key="vacation.search" bundle="${lang}"/></h1>
        <hr>
        <p id="message">${message}</p>

        <h2>
          <fmt:message key="h4.description.search" bundle="${lang}"/> <input type="text" pattern="^\d+$"
                                                                             class="input"
                                                                             name="number"/>
          <input type="submit" class="control-button"
                 value="<fmt:message key="button.search" bundle="${lang}"/>"/>
          <input type="hidden" name="cmd" value="vacationsSearch">
          <br><h2></h2>
          <div class="myTable">
            <table>
              <tr>
                <td><fmt:message key="td.number" bundle="${lang}"/></td>
                <td><fmt:message key="input.adding.date" bundle="${lang}"/></td>
                <td><fmt:message key="td.sum" bundle="${lang}"/></td>
                <td><fmt:message key="input.vacation.start" bundle="${lang}"/> //<fmt:message key="input.vacation.end" bundle="${lang}"/></td>

              </tr>
              <c:forEach var="vacation" items="${vacationList}">
                <tr>
                  <td><c:out value="${vacation.id}"/></td>
                  <td><fmt:formatDate value="${vacation.date}" type="date" dateStyle="full" /></td>
                  <td><c:out value="${vacation.sum}"/></td>
                  <td><c:out value="${vacation.period}"/></td>

                </tr>
              </c:forEach>
            </table>
          </div>
          <div align="center">
            <form id="paginator" action="Control" method="get">

              <p class="paginationP">
                <input type="hidden" name="cmd" value="vacationsSearch">
                <input type="hidden" name="employeeNumber" value="${employeeNumber}">
                <c:forEach begin="1" end="${totalTablePages}" var="i">
                  <c:choose>
                    <c:when test="${currentTablePage eq i}">
                      ${i}
                    </c:when>
                    <c:otherwise>
                      <input type="submit" class="pagination-btn" name="tablePageToGo" value="${i}">
                    </c:otherwise>
                  </c:choose>
                </c:forEach>
              </p>

              <p class="paginationP">
                <fmt:message key="total" bundle="${lang}"/>: <c:out value="${totalEntries}"/>
              </p>
            </form>

          </div>
      </div>
    </form>

  </div>
</div>

</body>
</html>
