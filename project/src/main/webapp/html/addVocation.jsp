<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setBundle basename="${bundle}" var="lang"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/input.css">
    <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/buttons.css">
    <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/form.css">
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/hamburger-menu.js"></script>

</head>
<title><fmt:message key="title.add.vacation" bundle="${lang}"/></title>

<body>
<div id="pgcontainer">
    <header>
        <div id="navbar">
            <a href="#" class="menubtn">menu</a>

            <div id="hamburgermenu">
                <ul>
                    <li>
                        <form name="changeLangForm" action="Control" method="get">
                            <input type="hidden" name="cmd" value="changeLang">
                            <input type="hidden" name="path" value="html/addVocation.jsp">
                            <input type="submit" class="locale-btn" name="langBtn" value="ru_Ru">
                            <input type="submit" class="locale-btn" name="langBtn" value="en_En">
                        </form>
                    </li>
                    <jsp:include page="menu.jsp"/>

                </ul>
            </div>
        </div>
        <div class="overlay"></div>
    </header>

    <div id="content">
        <h1><fmt:message key="h2.add.vacation" bundle="${lang}"/></h1>
        <hr>
        <h2><fmt:message key="h4.description.add.vacation" bundle="${lang}"/></h2>
        <br>
        <p id="message">${message}</p>

        <div class="wrap-form">
            <form method="post" action="${pageContext.request.contextPath}/Control">
                <label><fmt:message key="td.number" bundle="${lang}"/></label>
                <input type="text" pattern="^\d+$" class="input" required="true" name="number"/>
                <label><fmt:message key="input.sum" bundle="${lang}"/></label>
                <input type="text" pattern="[0-9]*\.?[0-9]*" class="input" required="true" name="sum"/>
                <label><fmt:message key="input.adding.date" bundle="${lang}"/></label>
                <input type="date" class="input" required name="date"/>
                <label><fmt:message key="input.vacation.start" bundle="${lang}"/></label>
                <input type="date" class="input" required name="startDate"/>
                <label> <fmt:message key="input.vacation.end" bundle="${lang}"/></label>
                <input type="date" class="input" required name="endDate"/>
                <input type="submit" class="control-button" value="<fmt:message key="button.add" bundle="${lang}"/>"/>
                <input type="hidden" name="cmd" value="addVacation">
            </form>
        </div>
    </div>
</div>

</body>
</html>