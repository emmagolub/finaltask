<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setBundle basename="${bundle}" var="lang"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/input.css">
    <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/buttons.css">
    <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/form.css">
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/updateEmpl.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/hamburger-menu.js"></script>

</head>
<title><fmt:message key="title.add.salary" bundle="${lang}"/></title>

<body>
<div id="pgcontainer">
    <header>
        <div id="navbar">
            <a href="#" class="menubtn">menu</a>

            <div id="hamburgermenu">
                <ul>
                    <li>
                        <form name="changeLangForm" action="Control" method="get">
                            <input type="hidden" name="cmd" value="changeLang">
                            <input type="hidden" name="path" value="html/addSalary.jsp">
                            <input type="submit" class="locale-btn" name="langBtn" value="ru_Ru">
                            <input type="submit" class="locale-btn" name="langBtn" value="en_En">
                        </form>
                    <li/>
                    <jsp:include page="menu.jsp"/>

                </ul>
            </div>
        </div>
        <div class="overlay"></div>
    </header>

    <div id="content">
        <h1><fmt:message key="h2.add.salary" bundle="${lang}"/></h1>
        <hr>
        <h2 style="line-height: 150%"><fmt:message key="h4.description.add.salary" bundle="${lang}"/></h2>
        <br>
        <p id="message">${message}</p>

        <div class="wrap-form">
            <form method="get" action="${pageContext.request.contextPath}/Control">
                <input type="submit" class="control-button" value="<fmt:message key="button.show" bundle="${lang}"/>"/>
                <input type="hidden" name="cmd" value="addSalaryInfo">
                <input type="hidden" name="ID" id="ID">
            </form>
            <form method="post" action="${pageContext.request.contextPath}/Control">
                <label><fmt:message key="input.number" bundle="${lang}"/>:</label>
                <input type="text" pattern="^\d+" class="input" required="true" value="${number}" id="number"
                       name="number"/>
                <label><fmt:message key="input.name" bundle="${lang}"/>: </label>
                <input type="text" class="input" value="${name}"/>
                <label><fmt:message key="input.department" bundle="${lang}"/>:</label>
                <input type="text" class="input" value="${department}"/>
                <label><fmt:message key="input.occupation" bundle="${lang}"/>: </label>
                <input type="text" class="input" value="${occupation}"/>

                <h2><fmt:message key="h4.adding" bundle="${lang}"/></h2><br>
                <label><fmt:message key="td.salary" bundle="${lang}"/></label>
                <input type="text" pattern="[0-9]*\.?[0-9]*" class="input" name="salary"/>
                <label><fmt:message key="td.hourly.payment" bundle="${lang}"/></label><br>
                <label><fmt:message key="input.hours.number" bundle="${lang}"/></label>
                <input type="text" pattern="^\d+" class="input" name="hoursNumber"/>
                <label><fmt:message key="input.hours.payment" bundle="${lang}"/></label>
                <input type="text" pattern="[0-9]*\.?[0-9]*" class="input" name="hourPayment"/>
                <label> <fmt:message key="input.vacation.payment" bundle="${lang}"/></label>
                <input type="text" pattern="[0-9]*\.?[0-9]*" class="input" name="vocationSurcharge"/>
                <label> <fmt:message key="input.overtime" bundle="${lang}"/></label>
                <input type="text" pattern="[0-9]*\.?[0-9]*" class="input" name="overLoadSurcharge"/>
                <label><fmt:message key="input.bonus" bundle="${lang}"/></label>
                <input type="text" pattern="[0-9]*\.?[0-9]*" class="input" name="bonus"/>

                <h2><fmt:message key="h4.recoupment" bundle="${lang}"/></h2><br>
                <label> <fmt:message key="input.fine" bundle="${lang}"/></label>
                <input type="text" pattern="[0-9]*\.?[0-9]*" class="input" name="fine"/>
                <label> <fmt:message key="input.prepayment" bundle="${lang}"/></label>
                <input type="text" pattern="[0-9]*\.?[0-9]*" class="input" name="prepayment"/>
                <label> <fmt:message key="input.profcontribution" bundle="${lang}"/></label>
                <input type="text" pattern="[0-9]*\.?[0-9]*" class="input" name="profContribution"/>
                <label><fmt:message key="input.adding.date" bundle="${lang}"/></label>
                <input type="date" class="input" name="date" required/>
                </tr
                </table>
                <input type="submit" class="control-button" value="<fmt:message key="button.add" bundle="${lang}"/>"/>
                <input type="hidden" name="cmd" value="addSalary">
            </form>
        </div>

    </div>
</div>

</body>
</html>
