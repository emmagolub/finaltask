<!--
  Created by IntelliJ IDEA.
  User: Эмма
  Date: 15.06.2016
  Time: 13:34
  To change this template use File | Settings | File Templates.
-->

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
  <fmt:setBundle basename="${bundle}" var="lang"/>
</head>

<body>

        <ul>

          <li>
            <form method="get" action="${pageContext.request.contextPath}/Control">
              <input type="submit" class="menu-btn"
                     value="<fmt:message key="title.all.employees" bundle="${lang}"/>"/>
              <input type="hidden" name="cmd" value="showEmployee">
            </form>
          </li>

          <li>
            <form method="get" action="${pageContext.request.contextPath}/Control">
              <input type="submit" class="menu-btn"
                     value="<fmt:message key="button.redirect.add.salary" bundle="${lang}"/>"/>
              <input type="hidden" name="cmd" value="redirectToAddSalary">
            </form>
          </li>

          <li>
            <form method="get" action="${pageContext.request.contextPath}/Control">
              <input type="submit" class="menu-btn"
                     value="<fmt:message key="button.add.prepayment" bundle="${lang}"/>"/>
              <input type="hidden" name="cmd" value="redirectToAddPrepayment">
            </form>
          </li>
          <li>
            <form method="get" action="${pageContext.request.contextPath}/Control">
              <input type="submit" class="menu-btn"
                     value="<fmt:message key="button.remove.employee" bundle="${lang}"/>"/>
              <input type="hidden" name="cmd" value="redirectToRemoveEmployee">
            </form>
          </li>
          <li>
            <form method="get" action="${pageContext.request.contextPath}/Control">
              <input type="submit" class="menu-btn"
                     value="<fmt:message key="button.add.vacation" bundle="${lang}"/>"/>
              <input type="hidden" name="cmd" value="redirectToAddVacation">
            </form>
          </li>
          <li>
            <form method="get" action="${pageContext.request.contextPath}/Control">
              <input type="submit" class="menu-btn"
                     value="<fmt:message key="button.add.employee" bundle="${lang}"/>"/>
              <input type="hidden" name="cmd" value="redirectToAddEmployee">
            </form>
          </li>
          <li>
            <form method="get" action="${pageContext.request.contextPath}/Control">
              <input type="submit" class="menu-btn"
                     value="<fmt:message key="button.update.employee" bundle="${lang}"/>"/>
              <input type="hidden" name="cmd" value="redirectToUpdateEmployee">
            </form>
          </li>
          <li>
            <form method="get" action="${pageContext.request.contextPath}/Control">
              <input type="submit" class="menu-btn"
                     value="<fmt:message key="button.show.dismissed" bundle="${lang}"/>"/>
              <input type="hidden" name="cmd" value="showDismissed">
            </form>
          </li>

          <li>
            <form method="get" action="${pageContext.request.contextPath}/Control">
              <input type="submit" class="menu-btn"
                     value="<fmt:message key="prepayment.history" bundle="${lang}"/>"/>
              <input type="hidden" name="cmd" value="showPrepayments">
            </form>
          </li>
          <li>
            <form method="get" action="${pageContext.request.contextPath}/Control">
              <input type="submit" class="menu-btn"
                     value="<fmt:message key="salary.history" bundle="${lang}"/>"/>
              <input type="hidden" name="cmd" value="showSalaries">
            </form>
          </li>
          <li>
            <form method="get" action="${pageContext.request.contextPath}/Control">
              <input type="submit" class="menu-btn"
                     value="<fmt:message key="vacation.history" bundle="${lang}"/>"/>
              <input type="hidden" name="cmd" value="showVacations">
            </form>
          </li>

          <li>
            <form method="get" action="${pageContext.request.contextPath}/Control">
              <input type="submit" class="menu-btn"
                     value="<fmt:message key="salary.search" bundle="${lang}"/>"/>
              <input type="hidden" name="cmd" value="redirectToSalariesSearch">
            </form>
          </li>

          <li>
            <form method="get" action="${pageContext.request.contextPath}/Control">
              <input type="submit" class="menu-btn"
                     value="<fmt:message key="prepayment.search" bundle="${lang}"/>"/>
              <input type="hidden" name="cmd" value="redirectToPrepaymentsSearch">
            </form>
          </li>

          <li>
            <form method="get" action="${pageContext.request.contextPath}/Control">
              <input type="submit" class="menu-btn"
                     value="<fmt:message key="vacation.search" bundle="${lang}"/>"/>
              <input type="hidden" name="cmd" value="redirectToVacationsSearch">
            </form>
          </li>
          <li>
            <form method="get" action="${pageContext.request.contextPath}/Control">
              <input type="submit" class="menu-btn"
                     value="<fmt:message key="button.logout" bundle="${lang}"/>"/>
              <input type="hidden" name="cmd" value="logOut">
            </form>
          </li>
        </ul>

</body>
</html>

