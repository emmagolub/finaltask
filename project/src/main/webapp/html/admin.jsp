<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setBundle basename="${bundle}" var="lang"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/info-table.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/input.css">
    <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/buttons.css">
    <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/form.css">
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/hamburger-menu.js"></script>
</head>
<title><fmt:message key="title.admin" bundle="${lang}"/></title>

<body>
<div id="pgcontainer">
    <header>
        <div id="navbar">
            <a href="#" class="menubtn">menu</a>

            <div id="hamburgermenu">
                <ul>
                    <li>
                        <form name="changeLangForm" action="Control" method="get">
                            <input type="hidden" name="cmd" value="changeLang">
                            <input type="hidden" name="path" value="html/admin.jsp">
                            <input type="submit" class="locale-btn" name="langBtn" value="ru_Ru">
                            <input type="submit" class="locale-btn" name="langBtn" value="en_En">
                        </form>
                    </li>

                    <li>
                        <form method="get" action="${pageContext.request.contextPath}/Control">
                            <input type="submit" class="menu-btn"
                                   value="<fmt:message key="button.logout" bundle="${lang}"/>"/>
                            <input type="hidden" name="cmd" value="logOut">
                        </form>
                    </li>
                </ul>
            </div>
        </div>
        <div class="overlay"></div>
    </header>

    <div id="content">
        <p id="message">${message}</p>

        <div class="wrap-form">
            <div id="center-block">
                <form id="add-user-form" method="post" action="${pageContext.request.contextPath}/Control">
                    <label><fmt:message key="input.login" bundle="${lang}"/>: </label>
                    <input type="text" class="input" name="loginToAdd"/>
                    <label><fmt:message key="input.password" bundle="${lang}"/>: </label>
                    <input type="password" class="input" name="password"/>
                    <input type="submit" class="control-button" value="<fmt:message key="button.add" bundle="${lang}"/>"/>
                    <input type="hidden" name="cmd" value="addUser">
                </form>
                <form id="lock-user-form" method="post" action="${pageContext.request.contextPath}/Control">
                    <label><fmt:message key="input.login" bundle="${lang}"/>: </label>
                    <input type="text" class="input" name="loginToLock"/>
                    <input type="submit" class="control-button" value="<fmt:message key="input.lock" bundle="${lang}"/>"/>
                    <input type="hidden" name="cmd" value="lockUser">
                </form>
                <form id="unlock-user-form" method="post" action="${pageContext.request.contextPath}/Control">
                    <label><fmt:message key="input.login" bundle="${lang}"/>: </label>
                    <input type="text" class="input" name="loginToUnlock"/>
                    <input type="submit" class="control-button" value="<fmt:message key="input.unlock" bundle="${lang}"/>"/>
                    <input type="hidden" name="cmd" value="unlockUser">
                </form>
            </div><h2></h2>
            <h2><fmt:message key="h4.users" bundle="${lang}"/></h2>
            <div class="myTable">
                <table>
                    <tr>
                        <td><fmt:message key="td.login" bundle="${lang}"/></td>

                        <td><fmt:message key="td.role" bundle="${lang}"/></td>
                        <td><fmt:message key="td.status" bundle="${lang}"/></td>
                    </tr>
                    <c:forEach var="account" items="${accountList}">
                        <tr>

                            <td><c:out value="${account.login}"/></td>
                            <td><c:out value="${account.roleName}"/></td>
                            <td><c:out value="${account.statusName}"/></td>

                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </div>
</div>

</body>
</html>