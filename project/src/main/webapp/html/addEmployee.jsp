<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setBundle basename="${bundle}" var="lang"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/input.css">
    <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/buttons.css">
    <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" type="text/css" media="all" href="${pageContext.request.contextPath}/css/form.css">
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/hamburger-menu.js"></script>

</head>
<title><fmt:message key="title.add.employee" bundle="${lang}"/></title>

<body>
<div id="pgcontainer">
    <header>
        <div id="navbar">
            <a href="#" class="menubtn">menu</a>

            <div id="hamburgermenu">
                <ul>
                    <li>
                        <form name="changeLangForm" action="Control" method="get">
                            <input type="hidden" name="cmd" value="changeLang">
                            <input type="hidden" name="path" value="html/addEmployee.jsp">
                            <input type="submit" class="locale-btn" name="langBtn" value="ru_Ru">
                            <input type="submit" class="locale-btn" name="langBtn" value="en_En">
                        </form>
                    </li>
                    <jsp:include page="menu.jsp"/>

                </ul>
            </div>
        </div>
        <div class="overlay"></div>
    </header>

    <div id="content">
        <h1><fmt:message key="h2.add.employee" bundle="${lang}"/></h1>
        <hr>
        <h2><fmt:message key="h4.description.add.employee" bundle="${lang}"/></h2>
        <br>
        <p id="message">${message}</p>

        <div class="wrap-form">


            <form method="post" action="${pageContext.request.contextPath}/Control">
                <label for="name"><fmt:message key="td.name" bundle="${lang}"/></label>
                <input type="text" class="input" id="name" name="name" required="true"/>


                <label for="department"><fmt:message key="td.department" bundle="${lang}"/></label>
                <input type="text" class="input" id="department" name="department" required="true"/>


                <label for="occupation"><fmt:message key="td.occupation" bundle="${lang}"/></label>
                <input type="text" class="input" id="occupation" name="occupation" required="true"/>


                <label for="date"> <fmt:message key="td.start.date" bundle="${lang}"/></label>
                <input type="date" id="date" class="input" name="date" required="true"/>


                <label for="children"><fmt:message key="td.children.number" bundle="${lang}"/>:</label>
                <input type="number" id = "children" pattern="^\d+" class="input" name="numberOfChildren" required="true"/>


                <label for="singleParent"><fmt:message key="td.single.parent" bundle="${lang}"/></label>
                <input type="checkbox" id="singleParent" class="input" name="singleParent" value="singleParent"/>


                <label for="cnpp"><fmt:message key="td.cnpp" bundle="${lang}"/></label>
                <input type="checkbox"  id="cnpp" class="input" name="cnpp" value="cnpp"/>

                <input type="submit" class="control-button" value="<fmt:message key="button.add" bundle="${lang}"/>"/>
                <input type="hidden" name="cmd" value="addEmployeeCmd">
            </form>
        </div>
    </div>
</div>

</body>
</html>